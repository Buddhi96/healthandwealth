import React, { Component } from 'react';

import 'aos/dist/aos.css';
class Two extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (  <div data-aos="fade-left">
        <img className="d-block w-100" src="https://res.cloudinary.com/sapiangroup/image/upload/f_auto,q_auto/v1598163582/OLLORUN_IMAGES/images/elements/slides-img/2_wylhsr.svg"
            alt="Second slide" />
        <div className="mobile-only">
            <h6>{this.props.lang[76]}</h6>
            <p>
            {this.props.lang[77]}</p>
        </div>
    </div>
     );
    }
}
 
export default Two;