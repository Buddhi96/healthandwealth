import React, { Component } from 'react';

import 'aos/dist/aos.css';
class Four extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (  <div data-aos="fade-left">
        <img className="d-block w-100" src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163580/OLLORUN_IMAGES/images/elements/slides-img/4_weow42.svg"
            alt="Third slide" />
        <div className="mobile-only">
            <h6>{this.props.lang[80]}</h6>
            <p>{this.props.lang[81]}</p>
        </div>
    </div>
    );
    }
}
 
export default Four;