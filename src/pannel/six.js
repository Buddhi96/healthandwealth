import React, { Component } from 'react';

import 'aos/dist/aos.css';
class Six extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (  <div data-aos="fade-left">
        <img className="d-block w-100" src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163584/OLLORUN_IMAGES/images/elements/slides-img/6_zwicum.svg"
            alt="Third slide" />
        <div className="mobile-only">
            <h6>{this.props.lang[84]}</h6>
            <p>{this.props.lang[85]}</p>
        </div>
    </div>
    );
    }
}
 
export default Six;