import React, { Component } from 'react';

import 'aos/dist/aos.css';
class Seven extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (    <div data-aos="fade-left">
        <img className="d-block w-100" src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163583/OLLORUN_IMAGES/images/elements/slides-img/7_dea9hl.svg"
            alt="Third slide" />
        <div className="mobile-only">
            <h6>{this.props.lang[86]}</h6>
            <p>{this.props.lang[87]}</p>
        </div>
    </div>
 );
    }
}
 
export default Seven;