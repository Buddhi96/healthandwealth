import React, { Component } from 'react';

import 'aos/dist/aos.css';
class One extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (  <div data-aos="fade-left" >
        <img className="d-block w-100" src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163576/OLLORUN_IMAGES/images/elements/slides-img/1_kchuzj.svg" alt="First slide" />
        <div className="mobile-only">
            <h6>{this.props.lang[74]}</h6>
            <p>
           {this.props.lang[75]}
            </p>
        </div>
    </div>
    );
    }
}
 
export default One;