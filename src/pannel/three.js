import React, { Component } from 'react';

import 'aos/dist/aos.css';

class Three extends Component {
  
    render() { 
        return (  <div data-aos="fade-left">
        <img className="d-block w-100" src={this.props.lang[149]}
            alt="Third slide" />
        <div className="mobile-only">
            <h6>{this.props.lang[78]}</h6>
            <p>{this.props.lang[79]}</p>
        </div>
    </div>
    );
    }
}
 
export default Three;