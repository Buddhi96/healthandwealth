import React from "react";
import Hselect from "./H_select";
import Wselect from "./W_select";
import Cookie_Accept from "./cookie_accept_panel";
import One from "../pannel/one";
import Two from "../pannel/two";
import Three from "../pannel/three";
import Four from "../pannel/four";
import Five from "../pannel/five";
import Six from "../pannel/six";
import Seven from "../pannel/seven";
import { Link } from "react-scroll";
import en from "../lang/en";
import fr from "../lang/fr";
import esp from "../lang/esp";
import AOS from "aos";
import "aos/dist/aos.css";
import SimpleParallax from "simple-parallax-js";
import Cookies from "universal-cookie";
const cookies = new Cookies();

const nextYear = new Date();
nextYear.setFullYear(new Date().getFullYear() + 10);
class Home extends React.Component {
  constructor(props) {
    super(props);

    if (cookies.get("Olloruncookie") === "MS") {
      this.state = {
        value: " ",
        divselected: "Home",
        num: 1,
        one: "#3120A9",
        two: "#FFFFFF",
        three: "#FFFFFF",
        four: "#FFFFFF",
        five: "#FFFFFF",
        six: "#FFFFFF",
        seven: "#FFFFFF",
        numchange: <One lang={en} />,
        onet: "#FFFFFF",
        one3: "#E52564",
        twot: "#3120A9",
        threet: "#3120A9",
        fourt: "#3120A9",
        fivet: "#3120A9",
        sixt: "#3120A9",
        sevent: "#3120A9",
        ln: en,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            UK{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
              alt=""
            />
          </a>
        ),
        stvalue: "",
        sivalue: "",
        prvalue: "",
        plvalue: "",
        cookieAccept: " ",
      };
    } else {
      this.state = {
        value: " ",
        num: 1,
        divselected: "Home",
        one: "#3120A9",
        two: "#FFFFFF",
        three: "#FFFFFF",
        four: "#FFFFFF",
        five: "#FFFFFF",
        six: "#FFFFFF",
        seven: "#FFFFFF",
        numchange: <One lang={en} />,
        onet: "#FFFFFF",
        one3: "#E52564",
        twot: "#3120A9",
        threet: "#3120A9",
        fourt: "#3120A9",
        fivet: "#3120A9",
        sixt: "#3120A9",
        sevent: "#3120A9",
        ln: en,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            UK{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
              alt=""
            />
          </a>
        ),
        stvalue: "",
        sivalue: "",
        prvalue: "",
        plvalue: "",
        cookieAccept: <Cookie_Accept set={this.setcookie} />,
      };
    }
  }
  setcookie = () => {
    this.setState({ cookieAccept: " " });
    cookies.set("Olloruncookie", "MS", { expires: nextYear });
  };

  handledivChange(letter) {
    if ("Home" === letter) {
      this.setState({
        value: " ",
        num: 1,
        divselected: "Home",
        one: "#3120A9",
        two: "#FFFFFF",
        three: "#FFFFFF",
        four: "#FFFFFF",
        five: "#FFFFFF",
        six: "#FFFFFF",
        seven: "#FFFFFF",
        numchange: <One lang={this.state.ln} />,
        onet: "#FFFFFF",
        one3: "#E52564",
        twot: "#3120A9",
        threet: "#3120A9",
        fourt: "#3120A9",
        fivet: "#3120A9",
        sixt: "#3120A9",
        sevent: "#3120A9",
        stvalue: "",
        sivalue: "",
        prvalue: "",
        plvalue: "",
      });
    } else if ("Hselect" === letter) {
      this.setState({
        value: <Hselect lang={this.state.ln} />,
        divselected: "Hselect",
      });
    } else if ("Wselect" === letter) {
      this.setState({
        value: <Wselect lang={this.state.ln} />,
        divselected: "Wselect",
      });
    }
  }
  changelang(lan) {
    if (lan === "en") {
      this.setState({
        ln: en,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            UK{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
              alt=""
            />
          </a>
        ),
      });
      if (this.state.num === 1) {
        this.setState({ numchange: <One lang={en} /> });
      } else if (this.state.num === 2) {
        this.setState({ numchange: <Two lang={en} /> });
      } else if (this.state.num === 3) {
        this.setState({ numchange: <Three lang={en} /> });
      } else if (this.state.num === 4) {
        this.setState({ numchange: <Four lang={en} /> });
      } else if (this.state.num === 5) {
        this.setState({ numchange: <Five lang={en} /> });
      } else if (this.state.num === 6) {
        this.setState({ numchange: <Six lang={en} /> });
      } else if (this.state.num === 7) {
        this.setState({ numchange: <Seven lang={en} /> });
      }
    } else if (lan === "fr") {
      this.setState({
        ln: fr,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            FR{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/FR_d2vo8g.svg"
              alt=""
            />
          </a>
        ),
      });
      if (this.state.num === 1) {
        this.setState({ numchange: <One lang={fr} /> });
      } else if (this.state.num === 2) {
        this.setState({ numchange: <Two lang={fr} /> });
      } else if (this.state.num === 3) {
        this.setState({ numchange: <Three lang={fr} /> });
      } else if (this.state.num === 4) {
        this.setState({ numchange: <Four lang={fr} /> });
      } else if (this.state.num === 5) {
        this.setState({ numchange: <Five lang={fr} /> });
      } else if (this.state.num === 6) {
        this.setState({ numchange: <Six lang={fr} /> });
      } else if (this.state.num === 7) {
        this.setState({ numchange: <Seven lang={fr} /> });
      }
    } else if (lan === "esp") {
      this.setState({
        ln: esp,
        flag: (
          <a
            className="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {" "}
            ESP{" "}
            <img
              className="ml-1"
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163562/OLLORUN_IMAGES/images/elements/ESP_ukit8i.svg"
              alt=""
            />
          </a>
        ),
      });
      if (this.state.num === 1) {
        this.setState({ numchange: <One lang={esp} /> });
      } else if (this.state.num === 2) {
        this.setState({ numchange: <Two lang={esp} /> });
      } else if (this.state.num === 3) {
        this.setState({ numchange: <Three lang={esp} /> });
      } else if (this.state.num === 4) {
        this.setState({ numchange: <Four lang={esp} /> });
      } else if (this.state.num === 5) {
        this.setState({ numchange: <Five lang={esp} /> });
      } else if (this.state.num === 6) {
        this.setState({ numchange: <Six lang={esp} /> });
      } else if (this.state.num === 7) {
        this.setState({ numchange: <Seven lang={esp} /> });
      }
    }
    if (this.state.divselected === "Home") {
      this.setState({
        value: " ",
        num: 1,
        divselected: "Home",
        one: "#3120A9",
        two: "#FFFFFF",
        three: "#FFFFFF",
        four: "#FFFFFF",
        five: "#FFFFFF",
        six: "#FFFFFF",
        seven: "#FFFFFF",
        numchange: <One lang={this.state.ln} />,
        onet: "#FFFFFF",
        one3: "#E52564",
        twot: "#3120A9",
        threet: "#3120A9",
        fourt: "#3120A9",
        fivet: "#3120A9",
        sixt: "#3120A9",
        sevent: "#3120A9",
        stvalue: "",
        sivalue: "",
        prvalue: "",
        plvalue: "",
      });
    } else if (this.state.divselected === "Hselect") {
      this.setState({
        value: <Wselect lang={this.state.ln} />,
        divselected: "Wselect",
      });
      setTimeout(
        function () {
          this.setState({
            value: <Hselect lang={this.state.ln} />,
            divselected: "Hselect",
          });
        }.bind(this),
        1
      );
    } else {
      this.setState({
        value: <Hselect lang={this.state.ln} />,
        divselected: "Hselect",
      });
      setTimeout(
        function () {
          this.setState({
            value: <Wselect lang={this.state.ln} />,
            divselected: "Wselect",
          });
        }.bind(this),
        1
      );
    }
  }
  selectpack(pack) {
    if ("STANDARD" === pack) {
      this.setState({
        stvalue: "#E52564",
        sivalue: "",
        prvalue: "",
        plvalue: "",
      });
    } else if ("SILVER" === pack) {
      this.setState({
        sivalue: "#E52564",
        stvalue: "",
        prvalue: "",
        plvalue: "",
      });
    } else if ("PREMIUM" === pack) {
      this.setState({
        prvalue: "#E52564",
        sivalue: "",
        stvalue: "",
        plvalue: "",
      });
    } else if ("PLATINUM" === pack) {
      this.setState({
        plvalue: "#E52564",
        sivalue: "",
        prvalue: "",
        stvalue: "",
      });
    }
  }
  handlepackChange(letter) {
    var html = document.getElementsByClassName("w-package-select")[0];

    if (html) {
      if (html.getAttribute("data") != letter) {
        document.getElementById("pack-" + letter).click();
        var packSecondNames = document.getElementsByClassName("package-name");
        for (var i = 0; i < packSecondNames.length; i++) {
          var packNameOne = packSecondNames[i];
          if (packNameOne.innerText === letter) {
            packNameOne.parentNode.parentNode
              .getElementsByClassName("select-button")[0]
              .click();
            packNameOne.parentNode.parentNode.getElementsByClassName(
              "dot"
            )[0].style.background = "rgb(229 37 100)";
          }
        }
      }
    } else {
      if ("STANDARD" === letter) {
        this.setState({
          value: <Wselect lang={this.state.ln} title="STANDARD" />,
        });
      } else if ("SILVER" === letter) {
        this.setState({
          value: <Wselect lang={this.state.ln} title="SILVER" />,
        });
      } else if ("PREMIUM" === letter) {
        this.setState({
          value: <Wselect lang={this.state.ln} title="PREMIUM" />,
        });
      } else if ("PLATINUM" === letter) {
        this.setState({
          value: <Wselect lang={this.state.ln} title="PLATINUM" />,
        });
      }
    }
  }
  Plusnum() {
    var number = this.state.num;
    var select = "#3120A9";
    var tselect = "#FFFFFF";
    var tunselect = "#000000";
    var accent = "#E52564";
    if (1 === number) {
      this.setState({
        num: number + 1,
        one: tselect,
        two: select,
        three: tselect,
        four: tselect,
        five: tselect,
        six: tselect,
        seven: tselect,
      });
      this.setState({
        onet: tunselect,
        twot: tselect,
        threet: tunselect,
        fourt: tunselect,
        fivet: tunselect,
        sixt: tunselect,
        sevent: tunselect,
      });
      this.setState({
        one3: tunselect,
        two3: accent,
        three3: tunselect,
        four3: tunselect,
        five3: tunselect,
        six3: tunselect,
        seven3: tunselect,
      });

      this.setState({ numchange: <Two lang={this.state.ln} /> });
    } else if (2 === number) {
      this.setState({
        num: number + 1,
        one: tselect,
        two: tselect,
        three: select,
        four: tselect,
        five: tselect,
        six: tselect,
        seven: tselect,
      });
      this.setState({
        onet: tunselect,
        twot: tunselect,
        threet: tselect,
        fourt: tunselect,
        fivet: tunselect,
        sixt: tunselect,
        sevent: tunselect,
      });
      this.setState({
        one3: tunselect,
        two3: tunselect,
        three3: accent,
        four3: tunselect,
        five3: tunselect,
        six3: tunselect,
        seven3: tunselect,
      });

      this.setState({ numchange: <Three lang={this.state.ln} /> });
    } else if (3 === number) {
      this.setState({
        num: number + 1,
        one: tselect,
        two: tselect,
        three: tselect,
        four: select,
        five: tselect,
        six: tselect,
        seven: tselect,
      });
      this.setState({
        onet: tunselect,
        twot: tunselect,
        threet: tunselect,
        fourt: tselect,
        fivet: tunselect,
        sixt: tunselect,
        sevent: tunselect,
      });
      this.setState({
        one3: tunselect,
        two3: tunselect,
        three3: tunselect,
        four3: accent,
        five3: tunselect,
        six3: tunselect,
        seven3: tunselect,
      });

      this.setState({ numchange: <Four lang={this.state.ln} /> });
    } else if (4 === number) {
      this.setState({
        num: number + 1,
        one: tselect,
        two: tselect,
        three: tselect,
        four: tselect,
        five: select,
        six: tselect,
        seven: tselect,
      });
      this.setState({
        onet: tunselect,
        twot: tunselect,
        threet: tunselect,
        fourt: tunselect,
        fivet: tselect,
        sixt: tunselect,
        sevent: tunselect,
      });
      this.setState({
        one3: tunselect,
        two3: tunselect,
        three3: tunselect,
        four3: tunselect,
        five3: accent,
        six3: tunselect,
        seven3: tunselect,
      });

      this.setState({ numchange: <Five lang={this.state.ln} /> });
    } else if (5 === number) {
      this.setState({
        num: number + 1,
        one: tselect,
        two: tselect,
        three: tselect,
        four: tselect,
        five: tselect,
        six: select,
        seven: tselect,
      });
      this.setState({
        onet: tunselect,
        twot: tunselect,
        threet: tunselect,
        fourt: tunselect,
        fivet: tunselect,
        sixt: tselect,
        sevent: tunselect,
      });
      this.setState({
        one3: tunselect,
        two3: tunselect,
        three3: tunselect,
        four3: tunselect,
        five3: tunselect,
        six3: accent,
        seven3: tunselect,
      });

      this.setState({ numchange: <Six lang={this.state.ln} /> });
    } else if (6 === number) {
      this.setState({
        num: number + 1,
        one: tselect,
        two: tselect,
        three: tselect,
        four: tselect,
        five: tselect,
        six: tselect,
        seven: select,
      });
      this.setState({
        onet: tunselect,
        twot: tunselect,
        threet: tunselect,
        fourt: tunselect,
        fivet: tunselect,
        sixt: tunselect,
        sevent: tselect,
      });
      this.setState({
        one3: tunselect,
        two3: tunselect,
        three3: tunselect,
        four3: tunselect,
        five3: tunselect,
        six3: tunselect,
        seven3: accent,
      });

      this.setState({ numchange: <Seven lang={this.state.ln} /> });
    } else if (7 === number) {
      this.setState({
        num: 1,
        one: select,
        two: tselect,
        three: tselect,
        four: tselect,
        five: tselect,
        six: tselect,
        seven: tselect,
      });
      this.setState({
        onet: tselect,
        twot: tunselect,
        threet: tunselect,
        fourt: tunselect,
        fivet: tunselect,
        sixt: tunselect,
        sevent: tunselect,
      });
      this.setState({
        one3: accent,
        two3: tunselect,
        three3: tunselect,
        four3: tunselect,
        five3: tunselect,
        six3: tunselect,
        seven3: tunselect,
      });

      this.setState({ numchange: <One lang={this.state.ln} /> });
    }
  }
  Minnum() {
    var number = this.state.num;
    var select = "#3120A9";
    var tselect = "#FFFFFF";
    var tunselect = "#000000";
    var accent = "#E52564";
    if (1 === number) {
      this.setState({
        num: 7,
        one: tselect,
        two: tselect,
        three: tselect,
        four: tselect,
        five: tselect,
        six: tselect,
        seven: select,
      });
      this.setState({
        onet: tunselect,
        twot: tunselect,
        threet: tunselect,
        fourt: tunselect,
        fivet: tunselect,
        sixt: tunselect,
        sevent: tselect,
      });
      this.setState({
        one3: tunselect,
        two3: tunselect,
        three3: tunselect,
        four3: tunselect,
        five3: tunselect,
        six3: tunselect,
        seven3: accent,
      });
      this.setState({ numchange: <Seven lang={this.state.ln} /> });
    } else if (2 === number) {
      this.setState({
        num: number - 1,
        one: select,
        two: tselect,
        three: tselect,
        four: tselect,
        five: tselect,
        six: tselect,
        seven: tselect,
      });
      this.setState({
        onet: tselect,
        twot: tunselect,
        threet: tunselect,
        fourt: tunselect,
        fivet: tunselect,
        sixt: tunselect,
        sevent: tunselect,
      });
      this.setState({
        one3: accent,
        two3: tunselect,
        three3: tunselect,
        four3: tunselect,
        five3: tunselect,
        six3: tunselect,
        seven3: tunselect,
      });
      this.setState({ numchange: <One lang={this.state.ln} /> });
    } else if (3 === number) {
      this.setState({
        num: number - 1,
        one: tselect,
        two: select,
        three: tselect,
        four: tselect,
        five: tselect,
        six: tselect,
        seven: tselect,
      });
      this.setState({
        onet: tunselect,
        twot: tselect,
        threet: tunselect,
        fourt: tunselect,
        fivet: tunselect,
        sixt: tunselect,
        sevent: tunselect,
      });
      this.setState({
        one3: tunselect,
        two3: accent,
        three3: tunselect,
        four3: tunselect,
        five3: tunselect,
        six3: tunselect,
        seven3: tunselect,
      });
      this.setState({ numchange: <Two lang={this.state.ln} /> });
    } else if (4 === number) {
      this.setState({
        num: number - 1,
        one: tselect,
        two: tselect,
        three: select,
        four: tselect,
        five: tselect,
        six: tselect,
        seven: tselect,
      });
      this.setState({
        onet: tunselect,
        twot: tunselect,
        threet: tselect,
        fourt: tunselect,
        fivet: tunselect,
        sixt: tunselect,
        sevent: tunselect,
      });
      this.setState({
        one3: tunselect,
        two3: tunselect,
        three3: accent,
        four3: tunselect,
        five3: tunselect,
        six3: tunselect,
        seven3: tunselect,
      });
      this.setState({ numchange: <Three lang={this.state.ln} /> });
    } else if (5 === number) {
      this.setState({
        num: number - 1,
        one: tselect,
        two: tselect,
        three: tselect,
        four: select,
        five: tselect,
        six: tselect,
        seven: tselect,
      });
      this.setState({
        onet: tunselect,
        twot: tunselect,
        threet: tunselect,
        fourt: tselect,
        fivet: tunselect,
        sixt: tunselect,
        sevent: tunselect,
      });
      this.setState({
        one3: tunselect,
        two3: tunselect,
        three3: tunselect,
        four3: accent,
        five3: tunselect,
        six3: tunselect,
        seven3: tunselect,
      });
      this.setState({ numchange: <Four lang={this.state.ln} /> });
    } else if (6 === number) {
      this.setState({
        num: number - 1,
        one: tselect,
        two: tselect,
        three: tselect,
        four: tselect,
        five: select,
        six: tselect,
        seven: tselect,
      });
      this.setState({
        onet: tunselect,
        twot: tunselect,
        threet: tunselect,
        fourt: tunselect,
        fivet: tselect,
        sixt: tunselect,
        sevent: tunselect,
      });
      this.setState({
        one3: tunselect,
        two3: tunselect,
        three3: tunselect,
        four3: tunselect,
        five3: accent,
        six3: tunselect,
        seven3: tunselect,
      });
      this.setState({ numchange: <Five lang={this.state.ln} /> });
    } else if (7 === number) {
      this.setState({
        num: number - 1,
        one: tselect,
        two: tselect,
        three: tselect,
        four: tselect,
        five: tselect,
        six: select,
        seven: tselect,
      });
      this.setState({
        onet: tunselect,
        twot: tunselect,
        threet: tunselect,
        fourt: tunselect,
        fivet: tunselect,
        sixt: tselect,
        sevent: tunselect,
      });
      this.setState({
        one3: tunselect,
        two3: tunselect,
        three3: tunselect,
        four3: tunselect,
        five3: tunselect,
        six3: accent,
        seven3: tunselect,
      });
      this.setState({ numchange: <Six lang={this.state.ln} /> });
    }
  }
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg fixed-top">
          <a className="navbar-brand" href="#">
            <img
              src="https://res.cloudinary.com/sapiangroup/image/upload/w_150,f_auto,q_auto/v1598163568/OLLORUN_IMAGES/images/elements/logo01_ollorun_ovoawo.png"
              alt=""
            />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span>
              <img
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/ham_y9sttw.svg"
                alt=""
              />
            </span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ml-auto">
              <li
                className="nav-item active"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <Link
                  to="top"
                  smooth={true}
                  duration={1000}
                  onClick={() => this.handledivChange("Home")}
                  className="nav-link"
                  href="#"
                >
                  {this.state.ln[0]} <span className="sr-only">(current)</span>
                </Link>
              </li>
              <li
                className="nav-item"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <Link
                  to="products"
                  smooth={true}
                  duration={1000}
                  className="nav-link"
                  href="#"
                >
                  {this.state.ln[1]}
                </Link>
              </li>
              <li
                className="nav-item"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <Link
                  to="opportunity"
                  smooth={true}
                  duration={1000}
                  className="nav-link"
                  href="#"
                >
                  {this.state.ln[2]}
                </Link>
              </li>
              <li
                className="nav-item"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  target="_blank"
                  smooth={true}
                  duration={1000}
                  className="nav-link"
                  href={this.state.ln[151]}
                >
                  {this.state.ln[150]}
                </a>
              </li>

              <li
                className="nav-item text-center"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  className="nav-link"
                  target="_BLANK"
                  href="https://training.ollorun.com/index"
                >
                  TRAINING
                </a>
              </li>

              <li
                className="nav-item text-center"
                data-toggle="collapse"
                data-target="#navbarNav"
              >
                <a
                  className="nav-link join-btn"
                  target="_BLANK"
                  href="http://store.ollorun.com/backoffice/"
                >
                  {this.state.ln[3]}
                </a>
              </li>

              <li className="nav-item dropdown">
                {this.state.flag}
                <div
                  className="dropdown-menu"
                  aria-labelledby="navbarDropdown"
                  data-toggle="collapse"
                  data-target="#navbarNav"
                >
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("en")}
                  >
                    UK{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163559/OLLORUN_IMAGES/images/elements/EN_xaxebt.svg"
                      alt=""
                    />
                  </a>
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("fr")}
                  >
                    FR{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163563/OLLORUN_IMAGES/images/elements/FR_d2vo8g.svg"
                      alt=""
                    />
                  </a>
                  <a
                    className="dropdown-item"
                    onClick={() => this.changelang("esp")}
                  >
                    ESP{" "}
                    <img
                      className="ml-1"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163562/OLLORUN_IMAGES/images/elements/ESP_ukit8i.svg"
                      alt=""
                    />
                  </a>
                </div>
              </li>
            </ul>
          </div>
        </nav>

        <section id="top" className="container-fluid">
          <div className="row sec-1 d-flex justify-content-center">
            <div className="col-md-12 logo d-flex justify-content-center">
              <div className="logo-data">
                <img
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_400,f_auto,q_auto/v1598163569/OLLORUN_IMAGES/images/elements/logo02_ollorun_df7xdq.png"
                  alt=""
                />
                <h4>{this.state.ln[4]}</h4>
              </div>
            </div>

            <div className="col-md-7 slider-1">
              <div
                id="carousel"
                className="carousel slide"
                data-ride="carousel"
              >
                <div className="carousel-inner">
                  <div className="carousel-item active">
                    <img
                      className="d-block w-100"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_800,f_auto,q_auto/v1598163489/OLLORUN_IMAGES/images/caroussel/caroussel_02_ollorun_sdrnf0.jpg"
                      alt="First slide"
                    />
                  </div>
                  <div className="carousel-item">
                    <img
                      className="d-block w-100"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_800,f_auto,q_auto/v1598163488/OLLORUN_IMAGES/images/caroussel/caroussel_03_ollorun_xklgm3.jpg"
                      alt="Second slide"
                    />
                  </div>
                  <div className="carousel-item">
                    <img
                      className="d-block w-100"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_800,f_auto,q_auto/v1598163494/OLLORUN_IMAGES/images/caroussel/caroussel_04_ollorun_ey6xls.jpg"
                      alt="Third slide"
                    />
                  </div>
                  <div className="carousel-item">
                    <img
                      className="d-block w-100"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_800,f_auto,q_auto/v1598163508/OLLORUN_IMAGES/images/caroussel/caroussel_05_ollorun_kewycf.jpg"
                      alt="Third slide"
                    />
                  </div>
                </div>
                <a
                  className="carousel-control-prev"
                  href="#carousel"
                  role="button"
                  data-slide="prev"
                >
                  <span
                    className="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">{this.state.ln[5]}</span>
                </a>
                <a
                  className="carousel-control-next"
                  href="#carousel"
                  role="button"
                  data-slide="next"
                >
                  <span
                    className="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">{this.state.ln[6]}</span>
                </a>
              </div>
            </div>
          </div>
        </section>

        {/* <!-- second section --> */}
        <section className="container-fluid">
          <div className="row sec-2-img-1 h-50 py-4 py-md-0">
            <div className="col-md-12 products-header  d-flex justify-content-center">
              <h5>{this.state.ln[7]}</h5>
              <div className="row d-flex justify-content-center product-packages">
                <div className="col-5 col-md-4 mt-4">
                  <Link to="changes" smooth={true} duration={1000}>
                    <div
                      className="product-package"
                      onClick={() => this.handledivChange("Wselect")}
                    >
                      <img
                        src="https://res.cloudinary.com/sapiangroup/image/upload/w_600,f_auto,q_auto/v1598163570/OLLORUN_IMAGES/images/elements/LogoW_Gold_ollorun_nqexrt.png"
                        alt=""
                      />
                    </div>
                  </Link>
                </div>

                <div className="col-5 col-md-4 mt-4">
                  <Link to="changes" smooth={true} duration={1000}>
                    <div
                      className="product-package"
                      onClick={() => this.handledivChange("Hselect")}
                    >
                      <img
                        src="https://res.cloudinary.com/sapiangroup/image/upload/w_600,f_auto,q_auto/v1598163569/OLLORUN_IMAGES/images/elements/LogoH_Blue_ollorun_d1xvzi.png"
                        alt=""
                      />
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <!-- third section --> */}
        {/* <!-- sections changes here --> */}
        <div id="changes">{this.state.value}</div>
        <section className="sec-change">
          <section className="container main-sec">
            <div className="row sec-3 d-flex justify-content-center">
              <div className="col-md-12">
                <h5>{this.state.ln[8]}</h5>
              </div>
              <div className="col-sm-12 mt-4 range-wrap">
                <h5 className="text-center text-md-left">
                  {this.state.ln[148]}
                </h5>
              </div>
              <div
                data-aos="fade-up"
                className="col-xl-3 col-sm-6 aos-init aos-animate"
              >
                <div className="package">
                  <div className="row">
                    <input type="hidden" className="number" value="126" />
                    <div className="col-sm-12 d-flex">
                      <div
                        className="dot"
                        style={{ background: this.state.stvalue }}
                      ></div>
                      <h6 className="wealth-package mx-auto">
                        {this.state.ln[9]}
                      </h6>
                    </div>
                    <div className="col text-center"></div>
                    <div className="col-sm-12">
                      <p className="package-name">{this.state.ln[12]}</p>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[10]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 yearprofit">630.63%</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[11]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 o-rate">0.137</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 mb-3 package-select">
                      <button
                        onClick={() => this.selectpack("STANDARD")}
                        className="select-button"
                      >
                        {this.state.ln[144]}
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div
                data-aos="fade-up"
                className="col-xl-3 col-sm-6 aos-init aos-animate"
              >
                <div className="package">
                  <div className="row">
                    <input type="hidden" className="number" value="335" />
                    <div className="col-sm-12 d-flex">
                      <div
                        className="dot"
                        style={{ background: this.state.sivalue }}
                      ></div>
                      <h6 className="wealth-package mx-auto">
                        {this.state.ln[9]}
                      </h6>
                    </div>
                    <div className="col-sm-12">
                      <p className="package-name">{this.state.ln[15]}</p>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[10]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 yearprofit">630.63%</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[11]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 o-rate">0.137</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 mb-3 package-select">
                      <button
                        onClick={() => this.selectpack("SILVER")}
                        className="select-button"
                      >
                        {this.state.ln[144]}
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div
                data-aos="fade-up"
                className="col-xl-3 col-sm-6 aos-init aos-animate"
              >
                <div className="package">
                  <div className="row">
                    <input type="hidden" className="number" value="620" />
                    <div className="col-sm-12 d-flex">
                      <div
                        className="dot"
                        style={{ background: this.state.prvalue }}
                      ></div>
                      <h6 className="wealth-package mx-auto">
                        {this.state.ln[9]}
                      </h6>
                    </div>
                    <div className="col-sm-12">
                      <p className="package-name">{this.state.ln[16]}</p>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[10]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 yearprofit">630.63%</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[11]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 o-rate">0.137</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 mb-3 package-select">
                      <button
                        onClick={() => this.selectpack("PREMIUM")}
                        className="select-button"
                      >
                        {this.state.ln[144]}
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div
                data-aos="fade-up"
                className="col-xl-3 col-sm-6 aos-init aos-animate"
              >
                <div className="package">
                  <div className="row">
                    <input type="hidden" className="number" value="1250" />
                    <div className="col-sm-12 d-flex">
                      <div
                        className="dot"
                        style={{ background: this.state.plvalue }}
                      ></div>
                      <h6 className="wealth-package mx-auto">
                        {this.state.ln[9]}
                      </h6>
                    </div>
                    <div className="col-sm-12">
                      <p className="package-name">{this.state.ln[18]}</p>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[10]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 yearprofit">630.63%</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 package-data">
                      <div className="row">
                        <div className="col">
                          <p className="ml-4">{this.state.ln[11]}</p>
                        </div>
                        <div className="col">
                          <p className="rate mr-4 o-rate">0.137</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-12 mb-3 package-select">
                      <button
                        onClick={() => this.selectpack("PLATINUM")}
                        className="select-button"
                      >
                        {this.state.ln[144]}
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-sm-12 mt-4 range-wrap range-wrap-main">
                <h5>{this.state.ln[20]}</h5>
                <input
                  id="range"
                  name="range"
                  className="range range-slider"
                  step="0.001"
                  type="range"
                  min="0.137"
                  max="1"
                />
                <output className="bubble"></output>
              </div>

              <div className="col-sm-12 yearstats mt-5">
                <div className="row d-flex justify-content-center">
                  {/*     <!-- stat --> */}
                  <div
                    className="col-sm-2 stat"
                    data-aos="fade-up"
                    data-aos-delay="50"
                    data-aos-duration="800"
                  >
                    <div className="row">
                      <div className="col-sm-12 days">
                        <h6 id="days">30 {this.state.ln[21]}</h6>
                      </div>

                      <div className="col-sm-12 percentage">
                        <h3 id="percentage">73.62%</h3>
                        <p className="my-3 text-center">{this.state.ln[145]}</p>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">Reward</span>
                        <span id="reward" className="value doller-value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">EUR</span>
                        <span id="usdt" className="value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">OZTG</span>
                        <span id="oztg" className="value">
                          43.32
                        </span>
                      </div>
                    </div>
                  </div>

                  {/* <!-- stat --> */}
                  <div
                    className="col-sm-2 stat"
                    data-aos="fade-up"
                    data-aos-delay="100"
                    data-aos-duration="800"
                  >
                    <div className="row">
                      <div className="col-sm-12 days">
                        <h6 id="days">90 {this.state.ln[21]}</h6>
                      </div>

                      <div className="col-sm-12 percentage">
                        <h3 id="percentage">73.62%</h3>
                        <p className="my-3 text-center">{this.state.ln[145]}</p>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">Reward</span>
                        <span id="reward" className="value doller-value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">EUR</span>
                        <span id="usdt" className="value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">OZTG</span>
                        <span id="oztg" className="value">
                          43.32
                        </span>
                      </div>
                    </div>
                  </div>

                  {/* <!-- stat --> */}
                  <div
                    className="col-sm-2 stat"
                    data-aos="fade-up"
                    data-aos-delay="150"
                    data-aos-duration="800"
                  >
                    <div className="row">
                      <div className="col-sm-12 days">
                        <h6 id="days">180 {this.state.ln[21]}</h6>
                      </div>

                      <div className="col-sm-12 percentage">
                        <h3 id="percentage">73.62%</h3>
                        <p className="my-3 text-center">{this.state.ln[145]}</p>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">Reward</span>
                        <span id="reward" className="value doller-value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">EUR</span>
                        <span id="usdt" className="value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">OZTG</span>
                        <span id="oztg" className="value">
                          43.32
                        </span>
                      </div>
                    </div>
                  </div>

                  {/*  <!-- stat --> */}
                  <div
                    className="col-sm-2 stat"
                    data-aos="fade-up"
                    data-aos-delay="200"
                    data-aos-duration="800"
                  >
                    <div className="row">
                      <div className="col-sm-12 days">
                        <h6 id="days">360 {this.state.ln[21]}</h6>
                      </div>

                      <div className="col-sm-12 percentage">
                        <h3 id="percentage">73.62%</h3>
                        <p className="my-3 text-center">{this.state.ln[145]}</p>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">Reward</span>
                        <span id="reward" className="value doller-value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">EUR</span>
                        <span id="usdt" className="value">
                          43.32
                        </span>
                      </div>

                      <div className="col-sm-12 stat-data">
                        <span className="data-name">OZTG</span>
                        <span id="oztg" className="value">
                          43.32
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          {/*  <!-- packs --> */}
        </section>

        {/*  <!-- section 4 --> */}
        <section id="opportunity" className="container-fluid">
          <div className="row sec-4 justify-content-center">
            <div className="col-sm-8">
              <h6>{this.state.ln[2]}</h6>
              <h5 className="text-justify" style={{ textAlignLast: "center" }}>
                {this.state.ln[22]}{" "}
              </h5>

              <img
                data-aos="fade-up"
                data-aos-delay="50"
                data-aos-duration="500"
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_1900,f_auto,q_auto/v1598163591/OLLORUN_IMAGES/images/elements/W_H_ollorun_ge6zoo.png"
                alt=""
              />
            </div>
          </div>
        </section>

        {/* <!-- section 5 --> */}
        <div className="container-fluid">
          <div className="row justify-content-center">
            <div className="col-sm-10">
              <section className="container-fluid">
                <div className="row justify-content-center sec-5 mx-1">
                  <div className="col-sm-5 d-flex justify-content-center">
                    <div className="my-auto">
                      <img
                        src="https://res.cloudinary.com/sapiangroup/image/upload/w_600,f_auto,q_auto/v1598163573/OLLORUN_IMAGES/images/elements/map_ollorun_z9pika.png"
                        alt=""
                      />
                    </div>
                  </div>

                  <div className="col-sm-1"></div>
                  <div className="col-sm-4">
                    <div className="row">
                      <div className="col-sm-12">
                        <h6 className="mt-4 mb-2 text-center text-md-left">
                          {this.state.ln[23]}
                        </h6>
                      </div>

                      <div className="col-sm-12">
                        <h2 className="text-center text-md-left mb-4">
                          {this.state.ln[24]}
                        </h2>

                        <p>{this.state.ln[25]} </p>
                        <p>{this.state.ln[26]}</p>
                        <p>{this.state.ln[27]}</p>
                        <br />
                        <br />
                        <p>{this.state.ln[28]} </p>
                        <p>{this.state.ln[29]}</p>
                        <p>{this.state.ln[30]}</p>
                        <p>{this.state.ln[31]}</p>
                        <p>{this.state.ln[32]}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>

        {/*  <!-- section 6 --> */}
        <section className="container-fluid mt-5">
          <div className="row sec-6">
            <div className="col-sm-1"></div>
            <div className="col-sm-5">
              <h6>{this.state.ln[33]}</h6>
              <h4>{this.state.ln[146]}</h4>
              <p>
                <span>{this.state.ln[34]} : </span>
                {this.state.ln[35]}{" "}
              </p>
              <p>▶{this.state.ln[36]}</p>
              <p>▶{this.state.ln[37]}</p>
              <p>▶{this.state.ln[38]}</p>
              <p>▶{this.state.ln[39]}</p>
              <p>▶{this.state.ln[40]}</p>

              <br />
              <br />
              <p>
                <span>{this.state.ln[41]} : </span> {this.state.ln[42]}
              </p>
              <p>▶{this.state.ln[43]}</p>
              <p>▶{this.state.ln[44]}</p>

              <p>▶{this.state.ln[46]}</p>
              <p>▶{this.state.ln[47]}</p>
            </div>

            <div className="col-sm-6 d-flex justify-content-center">
              <img
                data-aos="fade-up"
                data-aos-delay="50"
                data-aos-duration="500"
                className="mt-5 ml-3 ml-md-0"
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163588/OLLORUN_IMAGES/images/elements/W_ollorun_fmsgew.png"
                alt=""
              />
              <img
                data-aos="fade-up"
                data-aos-delay="100"
                data-aos-duration="500"
                className="mt-5 ml-3 ml-md-0"
                src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163565/OLLORUN_IMAGES/images/elements/H_ollorun_cqxoup.png"
                alt=""
              />
            </div>
          </div>
        </section>

        {/* <!-- section 7 --> */}
        <section className="container-fluid">
          <div className="row sec-7 mt-0 mt-md-5">
            <div className="col-sm-6 d-flex justify-content-center">
              <div className="my-auto">
                <img
                  className="w-100"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_600,f_auto,q_auto/v1598163584/OLLORUN_IMAGES/images/elements/Network_ollorun_i3kdid.png"
                  alt=""
                />
              </div>
            </div>

            <div className="col-sm-6">
              <div className="row">
                <div className="col-sm-6 text-center text-md-left mb-4">
                  <h6>{this.state.ln[48]}</h6>
                </div>
                <div className="col-sm-6 mt-2 text-center text-md-left mb-4">
                  <a
                    target="_BLANK"
                    href="https://store.ollorun.com/store/index.php?route=product/category&path=62"
                  >
                    {this.state.ln[49]}: 29€
                  </a>
                </div>

                <div className="col-sm-12 mt-4">
                  <h4>{this.state.ln[50]}</h4>
                  <p>{this.state.ln[51]}</p>
                  <p>▶{this.state.ln[52]}</p>
                  <p>▶ {this.state.ln[53]}</p>

                  <h4 className="mt-5">{this.state.ln[54]}</h4>
                  <p>{this.state.ln[55]}</p>
                  <div className="row">
                    <div className="col-sm-6">
                      <p>▶ {this.state.ln[56]}</p>
                      <p>▶ {this.state.ln[57]}</p>
                      <p>▶ {this.state.ln[58]}</p>
                      <p>▶ {this.state.ln[59]}</p>
                    </div>

                    <div className="col-sm-6 why d-flex justify-content-center">
                      <img
                        className="mt-5"
                        src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163565/OLLORUN_IMAGES/images/elements/icon01_ollorun_xlhip5.svg"
                        alt=""
                      />
                      <img
                        className="mt-5"
                        src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163565/OLLORUN_IMAGES/images/elements/icon02_ollorun_oy13zp.svg"
                        alt=""
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <!-- section 8 --> */}
        <section id="products" className="container-fluid mt-5">
          <div
            className="row sec-8 mx-1 justify-content-center"
            data-aos="fade-up"
            data-aos-delay="50"
            data-aos-duration="800"
          >
            <div className="col-sm-8 mb-5 text-center text-md-left">
              <h6>{this.state.ln[1]}</h6>
              <h4>{this.state.ln[61]}</h4>
            </div>

            <div className="col-sm-9">
              <div className="row product-packs">
                {/* <!-- wealth packs --> */}
                <div className="col-sm-12 w-packs">
                  <div className="row justify-content-center">
                    <div className="col-6 col-md-3 col-lg-3">
                      <a
                        href="https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=43"
                        target="_blank"
                      >
                        <div className="pack">
                          <img
                            src="https://res.cloudinary.com/sapiangroup/image/upload/w_400,f_auto,q_auto/v1598163542/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_standard_ollorun_bqslqa.png"
                            alt=""
                          />
                          <br />
                          <span>{this.state.ln[13]}</span>
                          <br />
                          <span className="price">€126 (PV 70)</span>
                        </div>
                      </a>
                    </div>

                    <div className="col-6 col-md-3 col-lg-3">
                      <a
                        href="https://store.ollorun.com/store/index.php?route=product/product&product_id=44"
                        target="_blank"
                      >
                        <div className="pack">
                          <img
                            src="https://res.cloudinary.com/sapiangroup/image/upload/w_400,f_auto,q_auto/v1598163545/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_silver_ollorun_hipyju.png"
                            alt=""
                          />
                          <br />
                          <span>{this.state.ln[15]}</span>
                          <br />
                          <span className="price">€335 (PV 150)</span>
                        </div>
                      </a>
                    </div>

                    <div className="col-6 col-md-3 col-lg-3">
                      <a
                        target="_blank"
                        href="https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=45"
                      >
                        <div className="pack">
                          <img
                            src="https://res.cloudinary.com/sapiangroup/image/upload/w_400,f_auto,q_auto/v1598163526/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_premium_ollorun_uzm4pw.png"
                            alt=""
                          />
                          <br />
                          <span>{this.state.ln[17]}</span>
                          <br />
                          <span className="price">€620 (PV 300)</span>
                        </div>
                      </a>
                    </div>

                    <div className="col-6 col-md-3 col-lg-3">
                      <a
                        target="_blank"
                        href="https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=46"
                      >
                        <div className="pack">
                          <img
                            src="https://res.cloudinary.com/sapiangroup/image/upload/w_400,f_auto,q_auto/v1598163539/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_platinum_ollorun_trcp28.png"
                            alt=""
                          />
                          <br />
                          <span>{this.state.ln[19]}</span>
                          <br />
                          <span className="price">€1250 (PV 600)</span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>

                {/*  <!-- Health packs --> */}
                <div className="col-sm-12 h-packs">
                  <div className="row d-flex justify-content-center">
                    <div className="col-6 col-md-3 col-lg-3">
                      <a
                        target="_blank"
                        href="https://store.ollorun.com/store/index.php?route=product/product&path=61&product_id=47"
                      >
                        <div className="pack">
                          <img
                            src="https://res.cloudinary.com/sapiangroup/image/upload/w_400,f_auto,q_auto/v1598163521/OLLORUN_IMAGES/images/img/Packs%20Health/Health_W_W_ollorun_wnvg6j.png"
                            alt=""
                          />
                          <br />
                          <span>{this.state.ln[63]}</span>
                          <br />
                          <span className="price">€149 (PV 75)</span>
                        </div>
                      </a>
                    </div>

                    <div className="col-6 col-md-3 col-lg-3">
                      <a
                        target="_blank"
                        href="https://store.ollorun.com/store/index.php?route=product/product&path=61&product_id=48"
                      >
                        <div className="pack">
                          <img
                            src="https://res.cloudinary.com/sapiangroup/image/upload/w_400,f_auto,q_auto/v1598163542/OLLORUN_IMAGES/images/img/Packs%20Health/Health_S_B_ollorun_wbv7gk.png"
                            alt=""
                          />
                          <br />
                          <span>{this.state.ln[65]}</span>
                          <br />
                          <span className="price">€199 (PV 100)</span>
                        </div>
                      </a>
                    </div>

                    <div className="col-6 col-md-3 col-lg-3">
                      <a
                        target="_blank"
                        href="https://store.ollorun.com/store/index.php?route=product/product&path=61&product_id=49"
                      >
                        <div className="pack">
                          <img
                            src="https://res.cloudinary.com/sapiangroup/image/upload/w_400,f_auto,q_auto/v1598163516/OLLORUN_IMAGES/images/img/Packs%20Health/Health_FY_ollorun_zchxhn.png"
                            alt=""
                          />
                          <br />
                          <span>{this.state.ln[67]}</span>
                          <br />
                          <span className="price">€399 (PV 200)</span>
                        </div>
                      </a>
                    </div>

                    <div className="col-6 col-md-3 col-lg-3">
                      <a
                        target="_blank"
                        href="https://store.ollorun.com/store/index.php?route=product/product&path=61&product_id=50"
                      >
                        <div className="pack">
                          <img
                            src="https://res.cloudinary.com/sapiangroup/image/upload/w_400,f_auto,q_auto/v1598163514/OLLORUN_IMAGES/images/img/Packs%20Health/Health_MB_ollorun_qqmgkz.png"
                            alt=""
                          />
                          <br />
                          <span>{this.state.ln[69]}</span>
                          <br />
                          <span className="price">€499 (PV 250)</span>
                        </div>
                      </a>
                    </div>
                  </div>
                  </div>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="container-fluid pil-packs">
            <div className="row justify-content-center">
              {/*  <!-- PIL packs --> */}
              <div className="col-sm-12">
                  <h4>Ollorun Mobile Full Node PIL</h4>
              </div>
              <div className="col-sm-8">
                <div className="row">
                  <div className="col-6 col-md-3 col-lg-3">
                    <a
                      href="https://store.ollorun.com/store/index.php?route=product/product&product_id=59"
                      target="_blank"
                    >
                      <div className="pack">
                        <img
                          src="https://res.cloudinary.com/dmeppscpx/image/upload/w_400,f_auto,q_auto/v1617525328/ollorun-pil-packs/STANDARD_PIL_MOBIL_3D_yyybtk.png"
                          alt=""
                        />
                        <br />
                        <span>STANDARD</span>
                        <br />
                        <span className="price">€375 (PV 100)</span>
                      </div>
                    </a>
                  </div>

                  <div className="col-6 col-md-3 col-lg-3">
                    <a
                      href="https://store.ollorun.com/store/index.php?route=product/product&product_id=64"
                      target="_blank"
                    >
                      <div className="pack">
                        <img
                          src="https://res.cloudinary.com/dmeppscpx/image/upload/w_400,f_auto,q_auto/v1617525327/ollorun-pil-packs/DUO_PIL_MOBIL_3D_yxdvu8.png"
                          alt=""
                        />
                        <br />
                        <span>DUO</span>
                        <br />
                        <span className="price">€700 (PV 200)</span>
                      </div>
                    </a>
                  </div>

                  <div className="col-6 col-md-3 col-lg-3">
                    <a
                      href="https://store.ollorun.com/store/index.php?route=product/product&product_id=61"
                      target="_blank"
                    >
                      <div className="pack">
                        <img
                          src="https://res.cloudinary.com/dmeppscpx/image/upload/w_400,f_auto,q_auto/v1617525328/ollorun-pil-packs/FAMILY_PIL_MOBIL_3D_doe0kh.png"
                          alt=""
                        />
                        <br />
                        <span>FAMILY</span>
                        <br />
                        <span className="price">€999 (PV 400)</span>
                      </div>
                    </a>
                  </div>

                  <div className="col-6 col-md-3 col-lg-3">
                    <a
                      href="https://store.ollorun.com/store/index.php?route=product/product&product_id=62"
                      target="_blank"
                    >
                      <div className="pack">
                        <img
                          src="https://res.cloudinary.com/dmeppscpx/image/upload/w_400,f_auto,q_auto/v1617525327/ollorun-pil-packs/ENTREPRENEUR_PIL_MOBIL_3D_kvvbcl.png"
                          alt=""
                        />
                        <br />
                        <span>ENTREPRENEUR</span>
                        <br />
                        <span className="price">€2,500 (PV 1 200)</span>
                      </div>
                    </a>
                  </div>
                  <div className="col-sm-12" style={{marginTop: "8rem"}}>
                  <h4>Ollorun Mobile Masternode PIL</h4>
              </div>
                  <div className="col-6 col-md-3 col-lg-3">
                    <a
                      href="https://store.ollorun.com/store/index.php?route=product/product&path=60_59&product_id=57"
                      target="_blank"
                    >
                      <div className="pack">
                        <img
                          src="https://res.cloudinary.com/dmeppscpx/image/upload/v1617535814/ollorun-pil-packs/PIL_STANDARD_ogvyrv.png"
                          alt=""
                        />
                        <br />
                        <span>STANDARD</span>
                        <br />
                        <span className="price">€3,269 (PV 375)</span>
                      </div>
                    </a>
                  </div>

                  <div className="col-6 col-md-3 col-lg-3">
                    <a
                      href="https://store.ollorun.com/store/index.php?route=product/product&path=60_59&product_id=56"
                      target="_blank"
                    >
                      <div className="pack">
                        <img
                          src="https://res.cloudinary.com/dmeppscpx/image/upload/v1617535814/ollorun-pil-packs/PIL_PREMIUM_z39kqp.png"
                          alt=""
                        />
                        <br />
                        <span>PREMIUM</span>
                        <br />
                        <span className="price">€16,9343 (PV 1 875)</span>
                      </div>
                    </a>
                  </div>

                  <div className="col-6 col-md-3 col-lg-3">
                    <a
                      href="https://store.ollorun.com/store/index.php?route=product/product&path=60_59&product_id=55"
                      target="_blank"
                    >
                      <div className="pack">
                        <img
                          src="https://res.cloudinary.com/dmeppscpx/image/upload/v1617535814/ollorun-pil-packs/PIL_GOLD_ukxrxx.png"
                          alt=""
                        />
                        <br />
                        <span>GOLD</span>
                        <br />
                        <span className="price">€32,685 (PV 3 800)</span>
                      </div>
                    </a>
                  </div>

                  <div className="col-6 col-md-3 col-lg-3">
                    <a
                      href="https://store.ollorun.com/store/index.php?route=product/product&path=60_59&product_id=32"
                      target="_blank"
                    >
                      <div className="pack">
                        <img
                          src="https://res.cloudinary.com/dmeppscpx/image/upload/v1617535814/ollorun-pil-packs/PIL_AMBASSADOR_pibnjm.png"
                          alt=""
                        />
                        <br />
                        <span>AMBASSADOR</span>
                        <br />
                        <span className="price">€65,370 (PV 7 600)</span>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <!-- pentagons  --> */}
        <section className="container-fluid pentagons">
          <div className="row sec-9">
            <div className="col-sm-12">
              <h6 className="topic">{this.state.ln[72]}</h6>
              <h4 className="mb-4 mt-4">{this.state.ln[73]}</h4>
            </div>

            <div className="col-sm-7 lg-only">
              {/* <!-- 1 --> */}
              <div className="row">
                <div className="col-sm-1 pentagon">
                  <svg
                    style={{ fill: this.state.one }}
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns="http://www.w3.org/1999/xlink"
                    xlink="http://www.w3.org/1999/xlink"
                    width="109.889"
                    height="105.979"
                    viewBox="0 0 109.889 105.979"
                  >
                    <defs>
                      <filter
                        id="Polygon_1"
                        x="0"
                        y="0"
                        width="109.889"
                        height="105.979"
                        filterUnits="userSpaceOnUse"
                      >
                        <feOffset dy="5" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="5" result="blur" />

                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                      </filter>
                    </defs>
                    <g
                      id="Group_1"
                      data-name="Group 1"
                      transform="translate(-104.056 -7983)"
                    >
                      <g
                        className="cls-3"
                        transform="matrix(1, 0, 0, 1, 104.06, 7983)"
                      >
                        <path
                          id="Polygon_1-2"
                          data-name="Polygon 1"
                          className="cls-1"
                          d="M42,0,81.944,29.021,66.687,75.979H17.313L2.056,29.021Z"
                          transform="translate(12.94 10)"
                        />
                      </g>
                      <text
                        style={{ fill: this.state.onet }}
                        id="_1"
                        data-name="1"
                        className="cls-2"
                        transform="translate(154 8041)"
                      >
                        <tspan x="0" y="0">
                          1
                        </tspan>
                      </text>
                    </g>
                  </svg>
                </div>

                <div className="col-sm-7 mt-4">
                  <h6 style={{ color: this.state.one3 }}>
                    {this.state.ln[74]}
                  </h6>
                  <p>{this.state.ln[75]}</p>
                </div>
              </div>

              {/* <!-- 2 --> */}
              <div className="row">
                <div className="col-sm-1 pentagon">
                  <svg
                    style={{ fill: this.state.two }}
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns="http://www.w3.org/1999/xlink"
                    xlink="http://www.w3.org/1999/xlink"
                    width="109.889"
                    height="105.979"
                    viewBox="0 0 109.889 105.979"
                  >
                    <defs>
                      <filter
                        id="Polygon_1"
                        x="0"
                        y="0"
                        width="109.889"
                        height="105.979"
                        filterUnits="userSpaceOnUse"
                      >
                        <feOffset dy="5" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="5" result="blur" />

                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                      </filter>
                    </defs>
                    <g
                      id="Group_1"
                      data-name="Group 1"
                      transform="translate(-104.056 -7983)"
                    >
                      <g
                        className="cls-3"
                        transform="matrix(1, 0, 0, 1, 104.06, 7983)"
                      >
                        <path
                          id="Polygon_1-2"
                          data-name="Polygon 1"
                          className="cls-1"
                          d="M42,0,81.944,29.021,66.687,75.979H17.313L2.056,29.021Z"
                          transform="translate(12.94 10)"
                        />
                      </g>
                      <text
                        style={{ fill: this.state.twot }}
                        id="_1"
                        data-name="1"
                        className="cls-2"
                        transform="translate(154 8041)"
                      >
                        <tspan x="0" y="0">
                          2
                        </tspan>
                      </text>
                    </g>
                  </svg>
                </div>

                <div className="col-sm-7 mt-4">
                  <h6 style={{ color: this.state.two3 }}>
                    {this.state.ln[76]}
                  </h6>
                  <p>{this.state.ln[77]}</p>
                </div>
              </div>

              {/* <!-- 3 --> */}
              <div className="row">
                <div className="col-sm-1 pentagon">
                  <svg
                    style={{ fill: this.state.three }}
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns="http://www.w3.org/1999/xlink"
                    xlink="http://www.w3.org/1999/xlink"
                    width="109.889"
                    height="105.979"
                    viewBox="0 0 109.889 105.979"
                  >
                    <defs>
                      <filter
                        id="Polygon_1"
                        x="0"
                        y="0"
                        width="109.889"
                        height="105.979"
                        filterUnits="userSpaceOnUse"
                      >
                        <feOffset dy="5" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="5" result="blur" />

                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                      </filter>
                    </defs>
                    <g
                      id="Group_1"
                      data-name="Group 1"
                      transform="translate(-104.056 -7983)"
                    >
                      <g
                        className="cls-3"
                        transform="matrix(1, 0, 0, 1, 104.06, 7983)"
                      >
                        <path
                          id="Polygon_1-2"
                          data-name="Polygon 1"
                          className="cls-1"
                          d="M42,0,81.944,29.021,66.687,75.979H17.313L2.056,29.021Z"
                          transform="translate(12.94 10)"
                        />
                      </g>
                      <text
                        style={{ fill: this.state.threet }}
                        id="_1"
                        data-name="1"
                        className="cls-2"
                        transform="translate(154 8041)"
                      >
                        <tspan x="0" y="0">
                          3
                        </tspan>
                      </text>
                    </g>
                  </svg>
                </div>

                <div className="col-sm-7 mt-4">
                  <h6 style={{ color: this.state.three3 }}>
                    {this.state.ln[78]}
                  </h6>
                  <p>{this.state.ln[79]}</p>
                </div>
              </div>

              {/* <!-- 4 --> */}
              <div className="row">
                <div className="col-sm-1 pentagon">
                  <svg
                    style={{ fill: this.state.four }}
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns="http://www.w3.org/1999/xlink"
                    xlink="http://www.w3.org/1999/xlink"
                    width="109.889"
                    height="105.979"
                    viewBox="0 0 109.889 105.979"
                  >
                    <defs>
                      <filter
                        id="Polygon_1"
                        x="0"
                        y="0"
                        width="109.889"
                        height="105.979"
                        filterUnits="userSpaceOnUse"
                      >
                        <feOffset dy="5" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="5" result="blur" />

                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                      </filter>
                    </defs>
                    <g
                      id="Group_1"
                      data-name="Group 1"
                      transform="translate(-104.056 -7983)"
                    >
                      <g
                        className="cls-3"
                        transform="matrix(1, 0, 0, 1, 104.06, 7983)"
                      >
                        <path
                          id="Polygon_1-2"
                          data-name="Polygon 1"
                          className="cls-1"
                          d="M42,0,81.944,29.021,66.687,75.979H17.313L2.056,29.021Z"
                          transform="translate(12.94 10)"
                        />
                      </g>
                      <text
                        style={{ fill: this.state.fourt }}
                        id="_1"
                        data-name="1"
                        className="cls-2"
                        transform="translate(154 8041)"
                      >
                        <tspan x="0" y="0">
                          4
                        </tspan>
                      </text>
                    </g>
                  </svg>
                </div>

                <div className="col-sm-7 mt-4">
                  <h6 style={{ color: this.state.four3 }}>
                    {this.state.ln[80]}
                  </h6>
                  <p>{this.state.ln[81]}</p>
                </div>
              </div>

              {/*  <!-- 5 --> */}
              <div className="row">
                <div className="col-sm-1 pentagon">
                  <svg
                    style={{ fill: this.state.five }}
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns="http://www.w3.org/1999/xlink"
                    xlink="http://www.w3.org/1999/xlink"
                    width="109.889"
                    height="105.979"
                    viewBox="0 0 109.889 105.979"
                  >
                    <defs>
                      <filter
                        id="Polygon_1"
                        x="0"
                        y="0"
                        width="109.889"
                        height="105.979"
                        filterUnits="userSpaceOnUse"
                      >
                        <feOffset dy="5" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="5" result="blur" />

                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                      </filter>
                    </defs>
                    <g
                      id="Group_1"
                      data-name="Group 1"
                      transform="translate(-104.056 -7983)"
                    >
                      <g
                        className="cls-3"
                        transform="matrix(1, 0, 0, 1, 104.06, 7983)"
                      >
                        <path
                          id="Polygon_1-2"
                          data-name="Polygon 1"
                          className="cls-1"
                          d="M42,0,81.944,29.021,66.687,75.979H17.313L2.056,29.021Z"
                          transform="translate(12.94 10)"
                        />
                      </g>
                      <text
                        style={{ fill: this.state.fivet }}
                        id="_1"
                        data-name="1"
                        className="cls-2"
                        transform="translate(154 8041)"
                      >
                        <tspan x="0" y="0">
                          5
                        </tspan>
                      </text>
                    </g>
                  </svg>
                </div>

                <div className="col-sm-7 mt-4">
                  <h6 style={{ color: this.state.five3 }}>
                    {this.state.ln[82]}
                  </h6>
                  <p>{this.state.ln[83]}</p>
                </div>
              </div>

              {/* <!-- 6 --> */}
              <div className="row">
                <div className="col-sm-1 pentagon">
                  <svg
                    style={{ fill: this.state.six }}
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns="http://www.w3.org/1999/xlink"
                    xlink="http://www.w3.org/1999/xlink"
                    width="109.889"
                    height="105.979"
                    viewBox="0 0 109.889 105.979"
                  >
                    <defs>
                      <filter
                        id="Polygon_1"
                        x="0"
                        y="0"
                        width="109.889"
                        height="105.979"
                        filterUnits="userSpaceOnUse"
                      >
                        <feOffset dy="5" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="5" result="blur" />

                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                      </filter>
                    </defs>
                    <g
                      id="Group_1"
                      data-name="Group 1"
                      transform="translate(-104.056 -7983)"
                    >
                      <g
                        className="cls-3"
                        transform="matrix(1, 0, 0, 1, 104.06, 7983)"
                      >
                        <path
                          id="Polygon_1-2"
                          data-name="Polygon 1"
                          className="cls-1"
                          d="M42,0,81.944,29.021,66.687,75.979H17.313L2.056,29.021Z"
                          transform="translate(12.94 10)"
                        />
                      </g>
                      <text
                        style={{ fill: this.state.sixt }}
                        id="_1"
                        data-name="1"
                        className="cls-2"
                        transform="translate(154 8041)"
                      >
                        <tspan x="0" y="0">
                          6
                        </tspan>
                      </text>
                    </g>
                  </svg>
                </div>

                <div className="col-sm-7 mt-4">
                  <h6 style={{ color: this.state.six3 }}>
                    {this.state.ln[84]}
                  </h6>
                  <p>{this.state.ln[85]} </p>
                </div>
              </div>

              {/* <!-- 7 --> */}
              <div className="row">
                <div className="col-sm-1 pentagon">
                  <svg
                    style={{ fill: this.state.seven }}
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns="http://www.w3.org/1999/xlink"
                    xlink="http://www.w3.org/1999/xlink"
                    width="109.889"
                    height="105.979"
                    viewBox="0 0 109.889 105.979"
                  >
                    <defs>
                      <filter
                        id="Polygon_1"
                        x="0"
                        y="0"
                        width="109.889"
                        height="105.979"
                        filterUnits="userSpaceOnUse"
                      >
                        <feOffset dy="5" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="5" result="blur" />

                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                      </filter>
                    </defs>
                    <g
                      id="Group_1"
                      data-name="Group 1"
                      transform="translate(-104.056 -7983)"
                    >
                      <g
                        className="cls-3"
                        transform="matrix(1, 0, 0, 1, 104.06, 7983)"
                      >
                        <path
                          id="Polygon_1-2"
                          data-name="Polygon 1"
                          className="cls-1"
                          d="M42,0,81.944,29.021,66.687,75.979H17.313L2.056,29.021Z"
                          transform="translate(12.94 10)"
                        />
                      </g>
                      <text
                        style={{ fill: this.state.sevent }}
                        id="_1"
                        data-name="1"
                        className="cls-2"
                        transform="translate(154 8041)"
                      >
                        <tspan x="0" y="0">
                          7
                        </tspan>
                      </text>
                    </g>
                  </svg>
                </div>

                <div className="col-sm-7 mt-4">
                  <h6 style={{ color: this.state.seven3 }}>
                    {this.state.ln[86]}
                  </h6>
                  <p>{this.state.ln[87]}</p>
                </div>
              </div>
            </div>

            {/* <!-- slider--> */}
            <div className="col-sm-5">
              <div className="row">
                {/* <!-- change div  --> */}
                <div
                  id="slide"
                  className="carousel slide"
                  data-ride="carousel"
                  data-interval="false"
                >
                  <div className="carousel-inner">{this.state.numchange}</div>
                </div>
              </div>

              <div className="row mt-5">
                <div className="col-sm-12">
                  <div className="row d-flex justify-content-center">
                    <a
                      className="mr-3"
                      onClick={() => this.Minnum()}
                      href="#slide"
                      role="button"
                      data-slide="prev"
                    >
                      <span aria-hidden="true">
                        <img
                          src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163520/OLLORUN_IMAGES/images/elements/arrow-with-circle-left_htoaon.svg"
                          alt=""
                        />
                      </span>
                      <span className="sr-only">Previous</span>
                    </a>

                    <a
                      className="ml-3"
                      onClick={() => this.Plusnum()}
                      href="#slide"
                      role="button"
                      data-slide="next"
                    >
                      <span aria-hidden="true">
                        <img
                          src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163526/OLLORUN_IMAGES/images/elements/arrow-with-circle-right_aaroms.svg"
                          alt=""
                        />
                      </span>
                      <span className="sr-only">Previous</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/*   <!-- section 10 --> */}
        <section
          className="container"
          data-aos="flip-left"
          data-aos-delay="50"
          data-aos-duration="800"
        >
          <div className="row sec-10">
            <div className="col-sm-6 d-flex justify-content-center">
              <div className="my-auto">
                <img
                  className="main-img"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163565/OLLORUN_IMAGES/images/elements/hand_lizfzu.png"
                  alt=""
                />
              </div>
            </div>

            <div className="col-sm-6">
              <div className="mt-4 text-center text-md-left">
                <h6>{this.state.ln[88]}</h6>
                <h4>{this.state.ln[89]}</h4>
              </div>

              <div className="row">
                <div className="col-sm-7 mt-4">
                  <p>▶ {this.state.ln[90]}</p>
                  <p>▶ {this.state.ln[91]}</p>
                  <p>▶ {this.state.ln[92]} </p>
                  <p className="last-p">▶ {this.state.ln[93]}</p>
                </div>

                <div className="col-sm-5 mt-4 d-flex justify-content-center">
                  <div className="mt-auto">
                    <img
                      className="socials"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163587/OLLORUN_IMAGES/images/elements/website_ollorun_aln6mu.svg"
                      alt=""
                    />
                    <img
                      className="socials"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163559/OLLORUN_IMAGES/images/elements/Discord_ollorun_lo34rw.svg"
                      alt=""
                    />
                    <img
                      className="socials"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163587/OLLORUN_IMAGES/images/elements/twitter_ollorun_ewokii.svg"
                      alt=""
                    />
                    <img
                      className="socials"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163562/OLLORUN_IMAGES/images/elements/facebook_ollorun_twllda.svg"
                      alt=""
                    />
                    <img
                      className="socials"
                      src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163568/OLLORUN_IMAGES/images/elements/instagram_ollorun_lekebj.svg"
                      alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <!-- section 10 slider 3 --> */}
        <section>
          <div
            className="container-fluid"
            data-aos="fade-up"
            data-aos-delay="50"
            data-aos-duration="800"
          >
            <div className="row sec-reward">
              <div className="col-sm-12 mb-3">
                <h6>{this.state.ln[94]}</h6>
              </div>
              {/* <!-- left --> */}
              <div className="col-sm-12">
                <div
                  id="slide-reward-badge"
                  className="carousel slide"
                  data-ride="carousel"
                  data-interval="false"
                >
                  <div className="carousel-inner d-flex justify-content-center">
                    <div className="carousel-item active">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="d-flex justify-content-center">
                            <div>
                              <img
                                className="d-block badge"
                                src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163584/OLLORUN_IMAGES/images/elements/Sapphire_ollorun_mehkcg.png"
                                alt="First slide"
                              />
                            </div>
                          </div>
                          <h2>6000</h2>
                          <h4>{this.state.ln[95]}</h4>
                          <p className="reward-name">{this.state.ln[96]}</p>
                        </div>

                        <div className="col-sm-6">
                          <div>
                            <img
                              className="w-100 reward-image"
                              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163557/OLLORUN_IMAGES/images/rewards/Smartphone_ollorun_dea7ta.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="carousel-item">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="d-flex justify-content-center">
                            <div>
                              <img
                                className="d-block badge"
                                src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163576/OLLORUN_IMAGES/images/elements/Ruby_ollorun_o4lilk.png"
                                alt="Second slide"
                              />
                            </div>
                          </div>
                          <h2>12 000</h2>
                          <h4>{this.state.ln[97]}</h4>
                          <p className="reward-name">{this.state.ln[98]}</p>
                        </div>

                        <div className="col-sm-6">
                          <div>
                            <img
                              className="w-100 reward-image"
                              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163557/OLLORUN_IMAGES/images/rewards/Macbook_ollorun_pi2zxu.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="carousel-item">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="d-flex justify-content-center">
                            <div>
                              <img
                                className="d-block badge"
                                src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163560/OLLORUN_IMAGES/images/elements/Emerald_ollorun_llwsit.png"
                                alt="Third slide"
                              />
                            </div>
                          </div>
                          <h2>24 000</h2>
                          <h4>{this.state.ln[99]}</h4>
                          <p>{this.state.ln[100]}</p>
                          <p className="reward-name">{this.state.ln[101]}</p>
                        </div>

                        <div className="col-sm-6">
                          <div>
                            <img
                              className="w-100 reward-image"
                              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163562/OLLORUN_IMAGES/images/rewards/Tesla_ollorun_mebwtm.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="carousel-item">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="d-flex justify-content-center">
                            <div>
                              <img
                                className="d-block badge"
                                src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163555/OLLORUN_IMAGES/images/elements/Diamond_ollorun_xsbiic.png"
                                alt="Third slide"
                              />
                            </div>
                          </div>
                          <h2>96 000</h2>
                          <h4>{this.state.ln[102]}</h4>
                          <p>{this.state.ln[103]}</p>
                          <p>{this.state.ln[104]}</p>
                          <p>{this.state.ln[105]}</p>
                          <p className="reward-name">{this.state.ln[106]}</p>
                        </div>

                        <div className="col-sm-6">
                          <div>
                            <img
                              className="w-100 reward-image"
                              src="https://res.cloudinary.com/sapiangroup/image/upload/w_600,f_auto,q_auto/v1598163561/OLLORUN_IMAGES/images/rewards/Travel_ollorun_ccopbt.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="carousel-item">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="d-flex justify-content-center">
                            <div>
                              <img
                                className="d-block badge"
                                src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163549/OLLORUN_IMAGES/images/elements/Blue_Diamond_ollorun_hsu5li.png"
                                alt="Third slide"
                              />
                            </div>
                          </div>
                          <h2>384 000</h2>
                          <h4>{this.state.ln[107]}</h4>
                          <p>{this.state.ln[105]}</p>
                          <p>{this.state.ln[100]}</p>
                          <p className="reward-name">{this.state.ln[108]}</p>
                        </div>

                        <div className="col-sm-6">
                          <div>
                            <img
                              className="w-100 reward-image"
                              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163558/OLLORUN_IMAGES/images/rewards/Supercar_ollorun_q2ect8.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="carousel-item">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="d-flex justify-content-center">
                            <div>
                              <img
                                className="d-block badge"
                                src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163544/OLLORUN_IMAGES/images/elements/Black_Diamond_ollorun_vyop4v.png"
                                alt="Third slide"
                              />
                            </div>
                          </div>
                          <h2>1 536 000</h2>
                          <h4>{this.state.ln[109]}</h4>
                          <p>{this.state.ln[105]}</p>
                          <p className="reward-name">{this.state.ln[110]}</p>
                        </div>

                        <div className="col-sm-6">
                          <div>
                            <img
                              className="w-100 reward-image"
                              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163556/OLLORUN_IMAGES/images/rewards/Gold_ollorun_oxzwfg.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="carousel-item">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="d-flex justify-content-center">
                            <div>
                              <img
                                className="d-block badge"
                                src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163558/OLLORUN_IMAGES/images/elements/Crown_Diamond_ollorun_yyzmr4.png"
                                alt="Third slide"
                              />
                            </div>
                          </div>
                          <h2>6 144 000</h2>
                          <h4>{this.state.ln[111]}</h4>
                          <p>{this.state.ln[112]}</p>
                          <p>{this.state.ln[100]}</p>
                          <p className="reward-name">{this.state.ln[113]}</p>
                        </div>

                        <div className="col-sm-6">
                          <div>
                            <img
                              className="w-100 reward-image"
                              src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163558/OLLORUN_IMAGES/images/rewards/House_ollorun_fzimcp.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="carousel-item">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="d-flex justify-content-center">
                            <div>
                              <img
                                className="d-block badge"
                                src="https://res.cloudinary.com/sapiangroup/image/upload/w_200,f_auto,q_auto/v1598163552/OLLORUN_IMAGES/images/elements/Crown_Ambassador_ollorun_bfgknq.png"
                                alt="Third slide"
                              />
                            </div>
                          </div>
                          <h2>24 576 000</h2>
                          <h4>{this.state.ln[114]}</h4>
                          <p>{this.state.ln[115]}</p>
                          <p>{this.state.ln[116]}</p>
                          <p>{this.state.ln[117]}</p>
                          <p>{this.state.ln[100]}</p>
                          <p className="reward-name">{this.state.ln[118]}</p>
                        </div>

                        <div className="col-sm-6">
                          <img
                            className="w-100 reward-image"
                            src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163556/OLLORUN_IMAGES/images/rewards/Boat_ollorun_yfow8w.jpg"
                            alt=""
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-sm-6 d-flex justify-content-center btns">
                <a
                  className="mr-3 p-2"
                  href="#slide-reward-badge"
                  role="button"
                  data-slide="prev"
                >
                  <span aria-hidden="true">
                    <img
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163520/OLLORUN_IMAGES/images/elements/arrow-with-circle-left_htoaon.svg"
                      alt=""
                    />
                  </span>
                  <span className="sr-only">Previous</span>
                </a>

                <a
                  className="ml-3 p-2"
                  href="#slide-reward-badge"
                  role="button"
                  data-slide="next"
                >
                  <span aria-hidden="true">
                    <img
                      src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163526/OLLORUN_IMAGES/images/elements/arrow-with-circle-right_aaroms.svg"
                      alt=""
                    />
                  </span>
                  <span className="sr-only">Previous</span>
                </a>
              </div>
            </div>
          </div>
        </section>

        {/* <!-- section 12 --> */}
        <section className="container-fluid mt-5">
          <div className="row sec-12 d-flex justify-content-center">
            <div className="col-sm-10 mt-5">
              <div className="row">
                <div className="col-sm-8 mb-5">
                  <h1>{this.state.ln[120]}</h1>
                </div>

                <div className="col-sm-4 mt-2">
                  <a
                    href="http://store.ollorun.com/backoffice/"
                    target="_BLANK"
                    className="join-btn"
                  >
                    {this.state.ln[3]}
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/*   <!-- footer --> */}
        <section className="container-fluid">
          <div className="row footer">
            <div className="col-sm-4 mt-2">
              <div className="col text-center text-md-left">
                <img
                  className="footer-logo"
                  src="https://res.cloudinary.com/sapiangroup/image/upload/w_auto,f_auto,q_auto/v1598163568/OLLORUN_IMAGES/images/elements/logo01_ollorun_ovoawo.png"
                  alt=""
                />
              </div>
              <div className="footer-social text-center text-md-left">
                <a href="https://discord.gg/k7BaPZVKjy" target="_blank">
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163584/OLLORUN_IMAGES/images/elements/social/discord_czzaml.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.instagram.com/ollorun_health/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605208415/OLLORUN_IMAGES/images/elements/social/instagram_health_g3rnti.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.instagram.com/ollorun_wealth/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605208564/OLLORUN_IMAGES/images/elements/social/instagram_wealth_lu9qpo.svg"
                    alt=""
                  />
                </a>
                <a href="https://twitter.com/Ollorun1?s=09" target="_blank">
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163586/OLLORUN_IMAGES/images/elements/social/twitter_qjpxf3.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.facebook.com/Ollorun-Network-112311330515922/"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163585/OLLORUN_IMAGES/images/elements/social/facebook_qhn2co.svg"
                    alt=""
                  />
                </a>
                <a
                  href="https://www.youtube.com/channel/UCKM2_TtWzVmLueKhdrSbEGQ"
                  target="_blank"
                >
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/v1605207157/OLLORUN_IMAGES/images/elements/social/youtube_jz4mah.svg"
                    alt=""
                  />
                </a>
              </div>
            </div>

            <div className="col-md-3 col-sm-6 mt-2">
              <ul>
                <li>
                  <Link to="top" smooth={true} duration={1000} href="#">
                    {this.state.ln[121]}
                  </Link>
                </li>
                <li>
                  <Link to="products" smooth={true} duration={1000} href="#">
                    {this.state.ln[122]}
                  </Link>
                </li>
                <li>
                  <Link to="opportunity" smooth={true} duration={1000} href="#">
                    {this.state.ln[123]}
                  </Link>
                </li>
                <li>
                  <Link to="members" smooth={true} duration={1000} href="#">
                    {this.state.ln[124]}
                  </Link>
                </li>
                <li>
                  <a
                    target="_blank"
                    href="https://store.ollorun.com/store/index.php?route=common/home"
                  >
                    {this.state.ln[125]}
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-md-3 col-sm-6 mt-2">
              <ul>
                <li>
                  <a
                    href="https://store.ollorun.com/store/index.php?route=information/information&information_id=5"
                    target="_blank"
                  >
                    {this.state.ln[126]}
                  </a>
                </li>
                <li>
                  <a
                    href="https://store.ollorun.com/store/index.php?route=information/information&information_id=3"
                    target="_blank"
                  >
                    {this.state.ln[127]}{" "}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[128]}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[129]}
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    {this.state.ln[130]}
                  </a>
                </li>
              </ul>
            </div>

            <div className="row text-center text-md-left">
              <div className="col-sm-12 mt-4">
                <p>{this.state.ln[131]}</p>
                <p>{this.state.ln[132]}</p>
                <p>
                  <img src="https://res.cloudinary.com/sapiangroup/image/upload/v1598163565/OLLORUN_IMAGES/images/elements/icon03_ollorun_tqiiju.svg" />
                  {this.state.ln[133]}
                </p>
              </div>
            </div>
          </div>
        </section>

        {this.state.cookieAccept}
      </div>
    );
  }

  componentDidMount() {
    AOS.init();
    var images = document.getElementsByClassName("parallax");
    new SimpleParallax(images, {
      delay: 0.6,
      transition: "cubic-bezier(0,0,0,1)",
    });

    const allRanges = document.querySelectorAll(".range-wrap-main");
    allRanges.forEach((wrap) => {
      const range = wrap.querySelector(".range");
      const bubble = wrap.querySelector(".bubble");
      range.value = 0;
      range.style.animation = "gradient 1s ease infinite";

      range.addEventListener("input", () => {
        setBubble(range, bubble);
      });

      setBubble(range, bubble);
    });

    function setBubble(range, bubble) {
      const val = range.value;
      const min = range.min ? range.min : 0;
      const max = range.max ? range.max : 100;
      const newVal = Number(((val - min) * 100) / (max - min));
      bubble.innerHTML = "$ " + val;

      const color =
        "linear-gradient(90deg, #360066 0%,  var(--main-color) " +
        newVal +
        "%, var(--main-light) " +
        newVal +
        "%)";
      range.style.background = color;

      // Sorta magic numbers based on size of the native UI thumb
      bubble.style.left = `calc(${newVal}% + (${8 - newVal * 0.15}px))`;
    }
  }
}
export default Home;
