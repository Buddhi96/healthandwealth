import React from 'react';


class W_select extends React.Component{
    shoot() {
        var packName = document.getElementsByClassName( "selected-wpack" )[0].innerText;
        var packSecondNames = document.getElementsByClassName( "package-name" );
        for( var i = 0; i < packSecondNames.length; i++ ){
            var packNameOne = packSecondNames[i];
            if( packNameOne.innerText ===packName ){
                packNameOne.parentNode.parentNode.getElementsByClassName("select-button")[0].click();
                packNameOne.parentNode.parentNode.getElementsByClassName("dot")[0].style.background = "rgb(229 37 100)";
            }
        }
    }

    
    constructor(props) {
        super(props);
            if("STANDARD"===this.props.title){
                this.state = { value: <img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163542/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_standard_ollorun_bqslqa.png" alt=""/>
                ,name:"STANDARD",price:"126",ln:this.props.lang,link:"https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=43"
        ,stclz:"col-sm-3 selected-btn-red selected-wpack"
        ,siclz:"col-sm-3 select-btns"
        ,prclz:"col-sm-3 select-btns"
        ,plclz:"col-sm-3 select-btns"

        ,clz30:"col-sm-3 selected-btn-red selected-wdays"
        ,clz90:"col-sm-3 select-btns"
        ,clz180:"col-sm-3 select-btns"
        ,clz360:"col-sm-3 select-btns"
            };
               
            }else if("SILVER"===this.props.title){
                this.state = { value: <img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163545/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_silver_ollorun_hipyju.png" alt=""/>
                ,name:"SILVER",price:"335",ln:this.props.lang,link:"https://store.ollorun.com/store/index.php?route=product/product&product_id=44"
                ,stclz:"col-sm-3 select-btns"
                ,siclz:"col-sm-3 selected-btn-red selected-wpack"
                ,prclz:"col-sm-3 select-btns"
                ,plclz:"col-sm-3 select-btns"
                ,clz30:"col-sm-3 selected-btn-red selected-wdays"
                ,clz90:"col-sm-3 select-btns"
                ,clz180:"col-sm-3 select-btns"
                ,clz360:"col-sm-3 select-btns"
            };
               
            }else if("PREMIUM"===this.props.title){
                this.state = {  value: <img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163526/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_premium_ollorun_uzm4pw.png" alt=""/>
                ,name:"PREMIUM",price:"620",ln:this.props.lang,link:"https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=45"
                ,stclz:"col-sm-3 select-btns"
        ,siclz:"col-sm-3 select-btns"
        ,prclz:"col-sm-3 selected-btn-red selected-wpack"
        ,plclz:"col-sm-3 select-btns"
        ,clz30:"col-sm-3 selected-btn-red selected-wdays"
        ,clz90:"col-sm-3 select-btns"
        ,clz180:"col-sm-3 select-btns"
        ,clz360:"col-sm-3 select-btns"
            };
               
            } else if("PLATINUM"===this.props.title){
                this.state = { value: <img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163539/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_platinum_ollorun_trcp28.png" alt=""/>
                ,name:"PLATINUM",price:"1250",ln:this.props.lang,link:"https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=46"
                ,stclz:"col-sm-3 select-btns"
        ,siclz:"col-sm-3 select-btns"
        ,prclz:"col-sm-3 select-btns"
        ,plclz:"col-sm-3 selected-btn-red selected-wpack" 
        ,clz30:"col-sm-3 selected-btn-red selected-wdays"
        ,clz90:"col-sm-3 select-btns"
        ,clz180:"col-sm-3 select-btns"
        ,clz360:"col-sm-3 select-btns"
            };
               
            } else{
                this.state = { value: <img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163542/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_standard_ollorun_bqslqa.png" alt=""/>
                ,name:"STANDARD",price:"126",ln:this.props.lang,link:"https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=43"
                ,stclz:"col-sm-3 selected-btn-red"
        ,siclz:"col-sm-3 select-btns"
        ,prclz:"col-sm-3 select-btns"
        ,plclz:"col-sm-3 select-btns"

        ,clz30:"col-sm-3 select-btns"
        ,clz90:"col-sm-3 selected-btn-red selected-wdays"
        ,clz180:"col-sm-3 select-btns"
        ,clz360:"col-sm-3 select-btns"
       
            };
          
            }
      }
      handledivChange(letter){
        var days = document.getElementsByClassName( "selected-wdays" )[0].innerText;
        if("STANDARD"===letter){
            this.setState({ value: <img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163542/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_standard_ollorun_bqslqa.png" alt=""/>
            ,name:"STANDARD",price:"126",link:"https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=43"
            ,stclz:"col-sm-3 selected-btn-red selected-wpack"
        ,siclz:"col-sm-3 select-btns"
        ,prclz:"col-sm-3 select-btns"
        ,plclz:"col-sm-3 select-btns"
        });
           if( days ==="30 DAYS" ){
                document.getElementById( "price" ).innerText = "- 129€";
           }else if(days ==="90 DAYS"){
                document.getElementById( "price" ).innerText = "- 140€";
           }else if( days ==="180 DAYS" ){
                document.getElementById( "price" ).innerText = "- 168€";
           }else if(days ==="360 DAYS"){
                document.getElementById( "price" ).innerText = "- 309€";
           }
           
        }else if("SILVER"===letter){
            this.setState({value: <img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163545/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_silver_ollorun_hipyju.png" alt=""/>
            ,name:"SILVER",price:"335",link:"https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=44"
            ,stclz:"col-sm-3 select-btns"
                ,siclz:"col-sm-3 selected-btn-red selected-wpack"
                ,prclz:"col-sm-3 select-btns"
                ,plclz:"col-sm-3 select-btns"
        });

            if( days ==="30 DAYS" ){
                document.getElementById( "price" ).innerText = "- 343€";
            }else if(days ==="90 DAYS"){
                document.getElementById( "price" ).innerText = "- 373€"; 
            }else if( days ==="180 DAYS" ){
                document.getElementById( "price" ).innerText = "- 446€"; 
            }else if(days ==="360 DAYS"){
                document.getElementById( "price" ).innerText = "- 823€"; 
            }
           
        }else if("PREMIUM"===letter){
            this.setState({ value: <img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163526/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_premium_ollorun_uzm4pw.png" alt=""/>
            ,name:"PREMIUM",price:"620",link:"https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=45"
            ,stclz:"col-sm-3 select-btns"
        ,siclz:"col-sm-3 select-btns"
        ,prclz:"col-sm-3 selected-btn-red selected-wpack"
        ,plclz:"col-sm-3 select-btns"
        });
            if( days ==="30 DAYS" ){
                document.getElementById( "price" ).innerText = "- 635€";
            }else if(days ==="90 DAYS"){
                document.getElementById( "price" ).innerText = "- 690€";
            }else if( days ==="180 DAYS" ){
                document.getElementById( "price" ).innerText = "- 826€";
            }else if(days ==="360 DAYS"){
                document.getElementById( "price" ).innerText = "- 1,523€";
            }
        } else if("PLATINUM"===letter){
            this.setState({value: <img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163539/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_platinum_ollorun_trcp28.png" alt=""/>
            ,name:"PLATINUM",price:"1250",link:"https://store.ollorun.com/store/index.php?route=product/product&path=60&product_id=46"
            ,stclz:"col-sm-3 select-btns"
        ,siclz:"col-sm-3 select-btns"
        ,prclz:"col-sm-3 select-btns"
        ,plclz:"col-sm-3 selected-btn-red selected-wpack"
        });
            if( days ==="30 DAYS" ){
                document.getElementById( "price" ).innerText = "- 1,280€";
            }else if(days ==="90 DAYS"){
                document.getElementById( "price" ).innerText = "- 1,392€";
            }else if( days ==="180 DAYS" ){
                document.getElementById( "price" ).innerText = "- 1,666€";
            }else if(days ==="360 DAYS"){
                document.getElementById( "price" ).innerText = "- 3,071€";
            }
        } 
    }
    handledayChange(days){
        if("30"===days){
            this.setState({ days:"30"
            ,clz30:"col-sm-3 selected-btn-red selected-wdays"
            ,clz90:"col-sm-3 select-btns"
            ,clz180:"col-sm-3 select-btns"
            ,clz360:"col-sm-3 select-btns"
        });
            if( this.state.name ==="STANDARD" ){
                document.getElementById( "price" ).innerText = "- 129€";
            }
            else if( this.state.name ==="SILVER" ){
                document.getElementById( "price" ).innerText = "- 343€"; 
            }else if( this.state.name ==="PREMIUM" ){
                document.getElementById( "price" ).innerText = "- 635€";
            }else if( this.state.name ==="PLATINUM" ){
                document.getElementById( "price" ).innerText = "- 1,280€";
            }
        }else if("90"===days){
            this.setState({days:"90"
            ,clz30:"col-sm-3 select-btns"
            ,clz90:"col-sm-3 selected-btn-red selected-wdays"
            ,clz180:"col-sm-3 select-btns"
            ,clz360:"col-sm-3 select-btns"
        });
            if( this.state.name ==="STANDARD" ){
                document.getElementById( "price" ).innerText = "- 140€";
            }
            else if( this.state.name ==="SILVER" ){
                document.getElementById( "price" ).innerText = "- 373€"; 
            }else if( this.state.name ==="PREMIUM" ){
                document.getElementById( "price" ).innerText = "- 690€";
            }else if( this.state.name ==="PLATINUM" ){
                document.getElementById( "price" ).innerText = "- 1,392€";
            }
        }else if("180"===days){
            this.setState({ days:"180"
            ,clz30:"col-sm-3 select-btns"
            ,clz90:"col-sm-3 select-btns"
            ,clz180:"col-sm-3 selected-btn-red selected-wdays"
            ,clz360:"col-sm-3 select-btns"
        });
            if( this.state.name ==="STANDARD" ){
                document.getElementById( "price" ).innerText = "- 168€";
            }
            else if( this.state.name ==="SILVER" ){
                document.getElementById( "price" ).innerText = "- 446€"; 
            }else if( this.state.name ==="PREMIUM" ){
                document.getElementById( "price" ).innerText = "- 826€";
            }else if( this.state.name ==="PLATINUM" ){
                document.getElementById( "price" ).innerText = "- 1,666€";
            }
        } else if("360"===days){
            this.setState({days:"360"
            ,clz30:"col-sm-3 select-btns"
            ,clz90:"col-sm-3 select-btns"
            ,clz180:"col-sm-3 select-btns"
            ,clz360:"col-sm-3 selected-btn-red selected-wdays"
        });
            if( this.state.name ==="STANDARD" ){
                document.getElementById( "price" ).innerText = "- 309€";
            }
            else if( this.state.name ==="SILVER" ){
                document.getElementById( "price" ).innerText = "- 823€"; 
            }else if( this.state.name ==="PREMIUM" ){
                document.getElementById( "price" ).innerText = "- 1,523€";
            }else if( this.state.name ==="PLATINUM" ){
                document.getElementById( "price" ).innerText = "- 3,071€";
            }  
        } 
    }
    render(){
        return(   <section className="sec-change w-package-select" data={this.state.name}>
            <div className="container-fluid sec-wealth">
            <div className="row">
                <div className="col-sm-12 mb-5">
                    <h6>{this.state.ln[134]}</h6>
                    <h4 className="text-center">{this.state.ln[136]}</h4>
                </div>
            </div>
        
            <div className="container" data-aos="fade-up">
                <div className="row">
                    <div className={this.state.stclz} onClick={() => this.handledivChange("STANDARD")}>
                        <a ><img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Wealth_icon_ollorun_jyntju.svg" alt=""/>STANDARD</a>
                    </div>
                    <div className={this.state.siclz} onClick={() => this.handledivChange("SILVER")}>
                        <a ><img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Wealth_icon_ollorun_jyntju.svg" alt=""/>SILVER</a>
                    </div>
                    <div className={this.state.prclz} onClick={() => this.handledivChange("PREMIUM")}>
                        <a ><img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Wealth_icon_ollorun_jyntju.svg" alt=""/>PREMIUM</a>
                    </div>
                    <div className={this.state.plclz} onClick={() => this.handledivChange("PLATINUM")}>
                        <a h><img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Wealth_icon_ollorun_jyntju.svg" alt=""/>PLATINUM</a>
                    </div>
                </div>
            </div>
            <br/><br/>
            <div className="col-sm-12 mb-5 mt-5">
                <h4 className="text-center mt-4">{this.state.ln[137]}</h4>
        
                <div className="container mt-4">
                    <div className="row">
                        <div className={this.state.clz30} onClick={() => this.handledayChange("30")}>
                            <a ><img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163545/OLLORUN_IMAGES/images/elements/calendar_iluuen.svg" alt=""/>30 {this.state.ln[138]}</a>
                        </div>
                        <div className={this.state.clz90}  onClick={() => this.handledayChange("90")}>
                            <a ><img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163545/OLLORUN_IMAGES/images/elements/calendar_iluuen.svg" alt=""/>90 {this.state.ln[138]}</a>
                        </div>
                        <div className={this.state.clz180}  onClick={() => this.handledayChange("180")}>
                            <a ><img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163545/OLLORUN_IMAGES/images/elements/calendar_iluuen.svg" alt=""/>180 {this.state.ln[138]}</a>
                        </div>
                        <div className={this.state.clz360}  onClick={() => this.handledayChange("360")}>
                            <a ><img src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163545/OLLORUN_IMAGES/images/elements/calendar_iluuen.svg" alt=""/>360 {this.state.ln[138]}</a>
                        </div>
                    </div>
                </div>
            </div>
        
        
            <div className="col-sm-12 mt-5">
                <div className="container">
                    <div className="row d-flex justify-content-center selected-pack">
                        <div className="col-sm-4 mt-4 mb-4 mb-md-0 text-center text-md-left">
                        {this.state.value}
                        </div>
        
                        <div className="col-sm-3 mt-auto">
                            <h4 className="accent">{this.state.ln[139]}</h4>
        
                            <div className="pack-details">
                                <p id="pack-name">{this.state.ln[140]} {this.state.name}</p>
                                <p id="price">- {this.state.price}€ </p>
                                <p id="days">- {this.state.days} {this.state.ln[141]} </p>
                                <a target="_BLANK" href={this.state.link}>{this.state.ln[142]}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        
        </div></section>
    );
    } 
}export default W_select