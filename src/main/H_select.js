import React from "react";

class H_select extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: (
        <img
          className="h-image"
          src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163549/OLLORUN_IMAGES/images/img/Thumbnails_Health/Vignette_FY_ollorun_gqftaz.jpg"
          alt=""
        />
      ),
      name: "FOREVER YOUNG",
      price: "399",
      ln: this.props.lang,
      link:
        "https://store.ollorun.com/store/index.php?route=product/product&path=61&product_id=49",
      fyclz: "col-sm-3 selected-btn-red",
      wwclz: "col-sm-3 select-btns",
      sbclz: "col-sm-3 select-btns",
      mdblz: "col-sm-3 select-btns",
      mnclz: "col-sm-3 select-btns",
      smclz: "col-sm-3 select-btns",
     dpclz: "col-sm-3 select-btns",
      ulclz: "col-sm-3 select-btns",
    };
  }
  handledivChange(letter) {
    if ("WW" === letter) {
      this.setState({
        value: (
          <img
            className="h-image"
            src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163549/OLLORUN_IMAGES/images/img/Thumbnails_Health/Vignette_WW_ollorun_zf8kvu.jpg"
            alt=""
          />
        ),
        name: "WHITE & WHITE",
        price: "149",
        ln: this.props.lang,
        link:
          "https://store.ollorun.com/store/index.php?route=product/product&path=61&product_id=47",
        fyclz: "col-sm-3 select-btns",
        wwclz: "col-sm-3 selected-btn-red",
        sbclz: "col-sm-3 select-btns",
        mdblz: "col-sm-3 select-btns",
        mnclz: "col-sm-3 select-btns",
        smclz: "col-sm-3 select-btns",
       dpclz: "col-sm-3 select-btns",
        ulclz: "col-sm-3 select-btns",
      });
    } else if ("SB" === letter) {
      this.setState({
        value: (
          <img
            className="h-image"
            src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163546/OLLORUN_IMAGES/images/img/Thumbnails_Health/Vignette_SB_ollorun_msf02t.jpg"
            alt=""
          />
        ),
        name: "SLIM & BUM",
        price: "199",
        ln: this.props.lang,
        link:
          "https://store.ollorun.com/store/index.php?route=product/product&path=61&product_id=48",
        fyclz: "col-sm-3 select-btns",
        wwclz: "col-sm-3 select-btns",
        sbclz: "col-sm-3 selected-btn-red",
        mdblz: "col-sm-3 select-btns",
        mnclz: "col-sm-3 select-btns",
        smclz: "col-sm-3 select-btns",
       dpclz: "col-sm-3 select-btns",
        ulclz: "col-sm-3 select-btns",
      });
    } else if ("FY" === letter) {
      this.setState({
        value: (
          <img
            className="h-image"
            src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163549/OLLORUN_IMAGES/images/img/Thumbnails_Health/Vignette_FY_ollorun_gqftaz.jpg"
            alt=""
          />
        ),
        name: "FOREVER YOUNG",
        price: "399",
        ln: this.props.lang,
        link:
          "https://store.ollorun.com/store/index.php?route=product/product&path=61&product_id=49",
        fyclz: "col-sm-3 selected-btn-red",
        wwclz: "col-sm-3 select-btns",
        sbclz: "col-sm-3 select-btns",
        mdblz: "col-sm-3 select-btns",
        mnclz: "col-sm-3 select-btns",
        smclz: "col-sm-3 select-btns",
       dpclz: "col-sm-3 select-btns",
        ulclz: "col-sm-3 select-btns",
      });
    } else if ("MDB" === letter) {
      this.setState({
        value: (
          <img
            className="h-image"
            src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163550/OLLORUN_IMAGES/images/img/Thumbnails_Health/Vignette_MDB_ollorun_snxafu.jpg"
            alt=""
          />
        ),
        name: "MY DAILY BEAUTY",
        price: "499",
        ln: this.props.lang,
        link:
          "https://store.ollorun.com/store/index.php?route=product/product&path=61&product_id=50",
        fyclz: "col-sm-3 select-btns",
        wwclz: "col-sm-3 select-btns",
        sbclz: "col-sm-3 select-btns",
        mdblz: "col-sm-3 selected-btn-red",
        mnclz: "col-sm-3 select-btns",
        smclz: "col-sm-3 select-btns",
       dpclz: "col-sm-3 select-btns",
        ulclz: "col-sm-3 select-btns",
      });
    } else if ("MN" === letter) {
        this.setState({
          value: (
            <img
              className="h-image"
              src="https://store.ollorun.com/store/image/catalog/new files png/H_png_520x520px 2/Ollorun_H_MICRONEEDLING.png"
              alt=""
            />
          ),
          name: "MICRONEEDLING",
          price: "129",
          ln: this.props.lang,
          link:
            "https://store.ollorun.com/store/index.php?route=product/product&product_id=29",
          fyclz: "col-sm-3 select-btns",
          wwclz: "col-sm-3 select-btns",
          sbclz: "col-sm-3 select-btns",
          mdblz: "col-sm-3 select-btns",
          mnclz: "col-sm-3 selected-btn-red",
          smclz: "col-sm-3 select-btns",
         dpclz: "col-sm-3 select-btns",
          ulclz: "col-sm-3 select-btns",
        });
      }else if ("SM" === letter) {
        this.setState({
          value: (
            <img
              className="h-image"
              src="https://store.ollorun.com/store/image/catalog/new files png/H_png_520x520px 2/Ollorun_H_MOISTURIZER.png"
              alt=""
            />
          ),
          name: "SUPER MOISTURIZER KIT",
          price: "60",
          ln: this.props.lang,
          link:
            "https://store.ollorun.com/store/index.php?route=product/product&product_id=28",
          fyclz: "col-sm-3 select-btns",
          wwclz: "col-sm-3 select-btns",
          sbclz: "col-sm-3 select-btns",
          mdblz: "col-sm-3 select-btns",
          mnclz: "col-sm-3 select-btns",
          smclz: "col-sm-3 selected-btn-red",
         dpclz: "col-sm-3 select-btns",
          ulclz: "col-sm-3 select-btns",
        });
      }else if ("DP" === letter) {
        this.setState({
          value: (
            <img
              className="h-image"
              src="https://store.ollorun.com/store/image/catalog/new%20files%20png/H_png_520x520px%202/Ollorun_H_DEMO.png"
              alt=""
            />
          ),
          name: "DEMO PACK",
          price: "129",
          ln: this.props.lang,
          link:
            "https://store.ollorun.com/store/index.php?route=product/product&product_id=58&search=Demo&category_id=0",
          fyclz: "col-sm-3 select-btns",
          wwclz: "col-sm-3 select-btns",
          sbclz: "col-sm-3 select-btns",
          mdblz: "col-sm-3 select-btns",
          mnclz: "col-sm-3 select-btns",
          smclz: "col-sm-3 select-btns",
         dpclz: "col-sm-3 selected-btn-red",
          ulclz: "col-sm-3 select-btns",
        });
      } else if ("UL" === letter) {
      this.setState({
        value: (
          <img
            className="h-image"
            src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163547/OLLORUN_IMAGES/images/img/Thumbnails_Health/Vignette_Ultimate_ollorun_wtaski.jpg"
            alt=""
          />
        ),
        name: "ULTIMATE",
        price: "499",
        ln: this.props.lang,
        link:
          "https://store.ollorun.com/store/index.php?route=product/product&path=61&product_id=51",
        fyclz: "col-sm-3 select-btns",
        wwclz: "col-sm-3 select-btns",
        sbclz: "col-sm-3 select-btns",
        mdblz: "col-sm-3 select-btns",
        mnclz: "col-sm-3 select-btns",
        smclz: "col-sm-3 select-btns",
       dpclz: "col-sm-3 select-btns",
        ulclz: "col-sm-3 selected-btn-red",
      });
    }
  }
  render() {
    return (
      <section className="sec-change">
        <div className="container-fluid sec-health">
          <div className="row">
            <div className="col-sm-12 mb-5">
              <h6>{this.state.ln[135]}</h6>
              <h4 className="text-center">{this.state.ln[136]}</h4>
            </div>
          </div>

          <div className="container" data-aos="fade-up">
            <div className="row d-flex justify-content-center">
              <div className={this.state.wwclz} onClick={() => this.handledivChange("WW")} >
                <a>
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Health_icon_ollorun_kaesrl.svg"
                    alt=""
                  />
                  {this.state.ln[62]}
                </a>
              </div>
              <div className={this.state.sbclz} onClick={() => this.handledivChange("SB")} >
                <a>
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Health_icon_ollorun_kaesrl.svg"
                    alt=""
                  />
                  {this.state.ln[64]}
                </a>
              </div>
              <div className={this.state.fyclz}onClick={() => this.handledivChange("FY")}>
                <a>
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Health_icon_ollorun_kaesrl.svg"
                    alt=""
                  />
                  {this.state.ln[66]}
                </a>
              </div>
              <div className={this.state.mdblz}onClick={() => this.handledivChange("MDB")} >
                <a>
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Health_icon_ollorun_kaesrl.svg"
                    alt=""
                  />
                  {this.state.ln[68]}
                </a>
              </div>
              <div className={this.state.mnclz} onClick={() => this.handledivChange("MN")} >
                <a>
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Health_icon_ollorun_kaesrl.svg"
                    alt=""
                  />
                  {this.state.ln[152]}
                </a>
              </div>
              <div className={this.state.smclz} onClick={() => this.handledivChange("SM")}>
                <a>
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Health_icon_ollorun_kaesrl.svg"
                    alt=""
                  />
                  {this.state.ln[153]}
                </a>
              </div>
              <div className={this.state.dpclz}  onClick={() => this.handledivChange("DP")} >
                <a>
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Health_icon_ollorun_kaesrl.svg"
                    alt=""
                  />
                  {this.state.ln[154]}
                </a>
              </div>
              <div  className={this.state.ulclz} onClick={() => this.handledivChange("UL")} >
                <a>
                  <img
                    src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163574/OLLORUN_IMAGES/images/elements/Ollorun_Health_icon_ollorun_kaesrl.svg"
                    alt=""
                  />
                  {this.state.ln[70]}
                </a>
              </div>
            </div>
          </div>

          <div className="col-sm-12 mt-5">
            <div className="container">
              <div className="row d-flex justify-content-center selected-pack">
                <div className="col-sm-4 mt-4 mb-4 mb-md-0 text-center text-md-left">
                  {this.state.value}
                </div>

                <div className="col-sm-3 mt-auto">
                  <h4 className="accent">{this.state.ln[139]}</h4>

                  <div className="pack-details">
                    <p id="pack-name">
                      {this.state.ln[143]} {this.state.name}
                    </p>
                    <p id="price">- {this.state.price}€ </p>

                    <a target="_BLANK" href={this.state.link}>
                      {this.state.ln[142]}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
export default H_select;
