import React from 'react';
import { Link } from 'react-scroll'

class WealthPackages extends React.Component {
    constructor(props) {
        super(props);
        this.state = { stvalue: "",sivalue: "",prvalue: "",plvalue: "" };

    }
    selectpack(pack){
        if ("STANDARD" === pack) {
            this.setState({ stvalue: "#E52564",sivalue:"", prvalue:"", plvalue:"" });

        } else if ("SILVER" === pack) {
            this.setState({ sivalue: "#E52564",stvalue: "",prvalue: "",plvalue: ""  });

        } else if ("PREMIUM" === pack) {
            this.setState({ prvalue:"#E52564",sivalue: "",stvalue: "",plvalue: "" });

        } else if ("PLATINUM" === pack) {
            this.setState({ plvalue: "#E52564",sivalue: "",prvalue: "",stvalue: "" });

        } 
    }
    render() {
        return (
            <section className="sec-change">
                <section className="container main-sec">
                    <div className="row sec-3 d-flex justify-content-center">

                        <div className="col-md-12">
                            <h5>OLLORUN WEALTH PACKAGES</h5>
                        </div>

                        <div data-aos="fade-up" className="col-xl-3 col-sm-6 aos-init aos-animate">
                            <div className="package">
                                <div className="row">
                                    <div className="col-sm-12 d-flex">
                                        <div className="dot" style={{background: this.state.stvalue }}></div>
                                        <h6 className="wealth-package mx-auto">wealth Package</h6>
                                    </div>
                                    <div className="col text-center">

                                    </div>
                                    <div className="col-sm-12">
                                        <p className="package-name">STANDARD</p>
                                    </div>
                                    <div className="col-sm-12 package-data">
                                        <div className="row">
                                            <div className="col">
                                                <p className="ml-4">Year Profit</p>
                                            </div>
                                            <div className="col">
                                                <p className="rate mr-4 yearprofit">630.63%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 package-data">
                                        <div className="row">
                                            <div className="col">
                                                <p className="ml-4">USDT Purchase price</p>
                                            </div>
                                            <div className="col">
                                                <p className="rate mr-4 o-rate">0.137</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 mb-3 package-select"><button  onClick={() => this.selectpack("STANDARD")} className="select-button">SELECT</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div data-aos="fade-up" className="col-xl-3 col-sm-6 aos-init aos-animate">
                            <div className="package">
                                <div className="row">
                                    <div className="col-sm-12 d-flex">
                                        <div  className="dot" style={{background: this.state.sivalue }}></div>
                                        <h6 className="wealth-package mx-auto">wealth Package</h6>
                                    </div>
                                    <div className="col-sm-12">
                                        <p className="package-name">SILVER</p>
                                    </div>
                                    <div className="col-sm-12 package-data">
                                        <div className="row">
                                            <div className="col">
                                                <p className="ml-4">Year Profit</p>
                                            </div>
                                            <div className="col">
                                                <p className="rate mr-4 yearprofit">630.63%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 package-data">
                                        <div className="row">
                                            <div className="col">
                                                <p className="ml-4">USDT Purchase price</p>
                                            </div>
                                            <div className="col">
                                                <p className="rate mr-4 o-rate">0.137</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 mb-3 package-select"><button onClick={() => this.selectpack("SILVER")} className="select-button">SELECT</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div data-aos="fade-up" className="col-xl-3 col-sm-6 aos-init aos-animate">
                            <div className="package">
                                <div className="row">
                                    <div className="col-sm-12 d-flex">
                                        <div className="dot" style={{background: this.state.prvalue }}></div>
                                        <h6 className="wealth-package mx-auto">wealth Package</h6>
                                    </div>
                                    <div className="col-sm-12">
                                        <p className="package-name">PREMIUM</p>
                                    </div>
                                    <div className="col-sm-12 package-data">
                                        <div className="row">
                                            <div className="col">
                                                <p className="ml-4">Year Profit</p>
                                            </div>
                                            <div className="col">
                                                <p className="rate mr-4 yearprofit">630.63%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 package-data">
                                        <div className="row">
                                            <div className="col">
                                                <p className="ml-4">USDT Purchase price</p>
                                            </div>
                                            <div className="col">
                                                <p className="rate mr-4 o-rate">0.137</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 mb-3 package-select"><button onClick={() => this.selectpack("PREMIUM")} className="select-button">SELECT</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div data-aos="fade-up" className="col-xl-3 col-sm-6 aos-init aos-animate">
                            <div className="package">
                                <div className="row">
                                    <div className="col-sm-12 d-flex">
                                        <div className="dot" style={{background: this.state.plvalue }}></div>
                                        <h6 className="wealth-package mx-auto">wealth Package</h6>
                                    </div>
                                    <div className="col-sm-12">
                                        <p className="package-name">PLATINUM</p>
                                    </div>
                                    <div className="col-sm-12 package-data">
                                        <div className="row">
                                            <div className="col">
                                                <p className="ml-4">Year Profit</p>
                                            </div>
                                            <div className="col">
                                                <p className="rate mr-4 yearprofit">630.63%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 package-data">
                                        <div className="row">
                                            <div className="col">
                                                <p className="ml-4">USDT Purchase price</p>
                                            </div>
                                            <div className="col">
                                                <p className="rate mr-4 o-rate">0.137</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 mb-3 package-select"><button onClick={() => this.selectpack("PLATINUM")} className="select-button">SELECT</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-12 mt-4 range-wrap">
                            <h5>Future OZTG Price</h5>
                            <input id="range" name="range" className="range range-slider" step="0.001" type="range" min="0.137" max="1" />
                            <output className="bubble"></output>
                        </div>


                        <div className="col-sm-12 yearstats mt-5">
                            <div className="row d-flex justify-content-center">
                                {/*     <!-- stat --> */}
                                <div className="col-sm-2 stat" data-aos="fade-up" data-aos-delay="50" data-aos-duration="800">
                                    <div className="row">
                                        <div className="col-sm-12 days">
                                            <h6>30 days</h6>
                                        </div>

                                        <div className="col-sm-12 percentage">
                                            <h3>73.62%</h3>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">Reward</span>
                                            <span className="value doller-value">43.32</span>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">USDT</span>
                                            <span className="value">43.32</span>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">OZTG</span>
                                            <span className="value">43.32</span>
                                        </div>
                                    </div>
                                </div>

                                {/* <!-- stat --> */}
                                <div className="col-sm-2 stat" data-aos="fade-up" data-aos-delay="100" data-aos-duration="800">
                                    <div className="row">
                                        <div className="col-sm-12 days">
                                            <h6>90 days</h6>
                                        </div>

                                        <div className="col-sm-12 percentage">
                                            <h3>73.62%</h3>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">Reward</span>
                                            <span className="value doller-value">43.32</span>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">USDT</span>
                                            <span className="value">43.32</span>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">OZTG</span>
                                            <span className="value">43.32</span>
                                        </div>
                                    </div>
                                </div>

                                {/* <!-- stat --> */}
                                <div className="col-sm-2 stat" data-aos="fade-up" data-aos-delay="150" data-aos-duration="800">
                                    <div className="row">
                                        <div className="col-sm-12 days">
                                            <h6>180 days</h6>
                                        </div>

                                        <div className="col-sm-12 percentage">
                                            <h3>73.62%</h3>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">Reward</span>
                                            <span className="value doller-value">43.32</span>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">USDT</span>
                                            <span className="value">43.32</span>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">OZTG</span>
                                            <span className="value">43.32</span>
                                        </div>
                                    </div>
                                </div>

                                {/*  <!-- stat --> */}
                                <div className="col-sm-2 stat" data-aos="fade-up" data-aos-delay="200" data-aos-duration="800">
                                    <div className="row">
                                        <div className="col-sm-12 days">
                                            <h6>360 days</h6>
                                        </div>

                                        <div className="col-sm-12 percentage">
                                            <h3>73.62%</h3>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">Reward</span>
                                            <span className="value doller-value">43.32</span>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">USDT</span>
                                            <span className="value">43.32</span>
                                        </div>

                                        <div className="col-sm-12 stat-data">
                                            <span className="data-name">OZTG</span>
                                            <span className="value">43.32</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                {/*  <!-- packs --> */}
                <section className="packs">
                    <div className="col-sm-12">

                        <div className="row d-flex justify-content-center">
                            <div className="col-sm-2">
                                <div className="pack" data-aos="fade-up" data-aos-delay="50">
                                <Link to="changes" smooth={true} duration={1000}>
                                <img onClick={this.props.standard} src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163542/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_standard_ollorun_bqslqa.png" alt="" />
                                </Link>
                                 </div>
                            </div>

                            <div className="col-sm-2">
                                <div className="pack" data-aos="fade-up" data-aos-delay="100">
                                <Link to="changes" smooth={true} duration={1000}>
                                    <img onClick={this.props.silver} src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163545/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_silver_ollorun_hipyju.png" alt="" />
                                    </Link>
                                </div>
                            </div>

                            <div className="col-sm-2">
                                <div className="pack" data-aos="fade-up" data-aos-delay="150">
                                <Link to="changes" smooth={true} duration={1000}>
                                    <img onClick={this.props.premium} src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163526/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_premium_ollorun_uzm4pw.png" alt="" />
                                    </Link>
                                </div>
                            </div>

                            <div className="col-sm-2">
                                <div className="pack" data-aos="fade-up" data-aos-delay="200">
                                <Link to="changes" smooth={true} duration={1000}>
                                    <img onClick={this.props.platinum} src="https://res.cloudinary.com/sapiangroup/image/upload/q_auto/v1598163539/OLLORUN_IMAGES/images/img/Packs%20Wealth/W_platinum_ollorun_trcp28.png" alt="" />
                                    </Link>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </section>);


    }
} export default WealthPackages